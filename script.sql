USE [master]
GO
/****** Object:  Database [dbProductShop]    Script Date: 13.03.2021 17:36:16 ******/
CREATE DATABASE [dbProductShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbProductShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbProductShop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbProductShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbProductShop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [dbProductShop] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbProductShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbProductShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbProductShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbProductShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbProductShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbProductShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbProductShop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbProductShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbProductShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbProductShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbProductShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbProductShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbProductShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbProductShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbProductShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbProductShop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbProductShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbProductShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbProductShop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbProductShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbProductShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbProductShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbProductShop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbProductShop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbProductShop] SET  MULTI_USER 
GO
ALTER DATABASE [dbProductShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbProductShop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbProductShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbProductShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbProductShop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbProductShop] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [dbProductShop] SET QUERY_STORE = OFF
GO
USE [dbProductShop]
GO
/****** Object:  Table [dbo].[BasketTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BasketTable](
	[BasketId] [int] IDENTITY(1,1) NOT NULL,
	[DeliveryId] [int] NOT NULL,
	[CountBasket] [int] NOT NULL,
 CONSTRAINT [PK_BasketTable] PRIMARY KEY CLUSTERED 
(
	[BasketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryTable](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[VATId] [int] NOT NULL,
 CONSTRAINT [PK_CategoryTable] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CheckTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CheckTable](
	[CheckId] [int] IDENTITY(1,1) NOT NULL,
	[DateCheck] [date] NOT NULL,
	[SellerId] [int] NOT NULL,
 CONSTRAINT [PK_CheckTable] PRIMARY KEY CLUSTERED 
(
	[CheckId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliveryTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryTable](
	[DeliveryId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[DateDelivery] [date] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[DateOfManufacture] [date] NOT NULL,
	[ShelfLife] [date] NOT NULL,
	[CountDelivery] [int] NOT NULL,
	[UnitId] [int] NOT NULL,
 CONSTRAINT [PK_DeliveryTable] PRIMARY KEY CLUSTERED 
(
	[DeliveryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductTable](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](100) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[VendorId] [int] NOT NULL,
 CONSTRAINT [PK_ProductTable] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleTable](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[NameRole] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RoleTable] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SaleTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaleTable](
	[SaleId] [int] IDENTITY(1,1) NOT NULL,
	[DeliveryId] [int] NOT NULL,
	[PriceWithoutDiscount] [int] NOT NULL,
	[CountSale] [int] NOT NULL,
	[CheckId] [int] NOT NULL,
	[Discount] [tinyint] NOT NULL,
	[VATSale] [tinyint] NOT NULL,
 CONSTRAINT [PK_SaleTable] PRIMARY KEY CLUSTERED 
(
	[SaleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnitTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnitTable](
	[UnitId] [int] IDENTITY(1,1) NOT NULL,
	[NameUnit] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UnitTable] PRIMARY KEY CLUSTERED 
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTable](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserLogin] [varchar](50) NOT NULL,
	[UserPassword] [nchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[NameUser] [varchar](100) NOT NULL,
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VATTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VATTable](
	[VATId] [int] IDENTITY(1,1) NOT NULL,
	[Percent] [tinyint] NOT NULL,
 CONSTRAINT [PK_VATTable] PRIMARY KEY CLUSTERED 
(
	[VATId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VendorTable]    Script Date: 13.03.2021 17:36:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VendorTable](
	[VendorId] [int] IDENTITY(1,1) NOT NULL,
	[VendorName] [varchar](50) NOT NULL,
	[ContactPerson] [varchar](50) NOT NULL,
	[PhonNumber] [varchar](12) NOT NULL,
	[Adress] [varchar](50) NOT NULL,
 CONSTRAINT [PK_VendorTable] PRIMARY KEY CLUSTERED 
(
	[VendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BasketTable] ON 

INSERT [dbo].[BasketTable] ([BasketId], [DeliveryId], [CountBasket]) VALUES (60, 8, 1)
INSERT [dbo].[BasketTable] ([BasketId], [DeliveryId], [CountBasket]) VALUES (62, 2, 5)
SET IDENTITY_INSERT [dbo].[BasketTable] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoryTable] ON 

INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (2, N'Хлебобулочные изделия', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (3, N'Плоды и овощи', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (4, N'Кондитерские изделия', 2)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (5, N'Мясные и колбасные изделия', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (6, N'Рыба и рыбные изделия', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (7, N'Яичные изделия', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (8, N'Пищевые жиры', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (9, N'Безалкогольные напитки', 2)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (11, N'Детское питание', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (12, N'Зерно-мучная продукция', 1)
INSERT [dbo].[CategoryTable] ([CategoryId], [CategoryName], [VATId]) VALUES (14, N'Молочно-масляная продукция', 1)
SET IDENTITY_INSERT [dbo].[CategoryTable] OFF
GO
SET IDENTITY_INSERT [dbo].[CheckTable] ON 

INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (8, CAST(N'2021-01-31' AS Date), 17)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (9, CAST(N'2021-02-01' AS Date), 18)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (11, CAST(N'2021-02-06' AS Date), 10)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (12, CAST(N'2021-02-07' AS Date), 9)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (25, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (29, CAST(N'2021-02-14' AS Date), 18)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (30, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (31, CAST(N'2021-02-14' AS Date), 18)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (32, CAST(N'2021-02-14' AS Date), 16)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (35, CAST(N'2021-02-14' AS Date), 17)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (36, CAST(N'2021-02-14' AS Date), 13)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (39, CAST(N'2021-02-14' AS Date), 10)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (40, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (41, CAST(N'2021-02-14' AS Date), 15)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (42, CAST(N'2021-02-14' AS Date), 17)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (43, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (44, CAST(N'2021-02-14' AS Date), 9)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (45, CAST(N'2021-02-14' AS Date), 16)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (46, CAST(N'2021-02-14' AS Date), 18)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (47, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (48, CAST(N'2021-02-14' AS Date), 9)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (49, CAST(N'2021-02-14' AS Date), 15)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (50, CAST(N'2021-02-14' AS Date), 16)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (51, CAST(N'2021-02-14' AS Date), 17)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (52, CAST(N'2021-02-14' AS Date), 10)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (53, CAST(N'2021-02-14' AS Date), 13)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (54, CAST(N'2021-02-14' AS Date), 21)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (55, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (56, CAST(N'2021-02-14' AS Date), 15)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (57, CAST(N'2021-02-14' AS Date), 21)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (58, CAST(N'2021-02-14' AS Date), 15)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (59, CAST(N'2021-02-14' AS Date), 9)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (61, CAST(N'2021-02-14' AS Date), 17)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (62, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (64, CAST(N'2021-02-14' AS Date), 21)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (65, CAST(N'2021-02-14' AS Date), 10)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (66, CAST(N'2021-02-14' AS Date), 17)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (67, CAST(N'2021-02-14' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (68, CAST(N'2021-02-14' AS Date), 9)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (69, CAST(N'2021-02-20' AS Date), 16)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (70, CAST(N'2021-02-20' AS Date), 1)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (73, CAST(N'2021-02-20' AS Date), 9)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (79, CAST(N'2021-02-20' AS Date), 15)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (80, CAST(N'2021-02-21' AS Date), 15)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (81, CAST(N'2021-03-01' AS Date), 13)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (82, CAST(N'2021-03-13' AS Date), 14)
INSERT [dbo].[CheckTable] ([CheckId], [DateCheck], [SellerId]) VALUES (83, CAST(N'2021-03-13' AS Date), 17)
SET IDENTITY_INSERT [dbo].[CheckTable] OFF
GO
SET IDENTITY_INSERT [dbo].[DeliveryTable] ON 

INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (2, 2, CAST(N'2021-02-02' AS Date), 30.0000, CAST(N'2020-12-21' AS Date), CAST(N'2021-06-21' AS Date), 2, 1)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (3, 3, CAST(N'2021-01-31' AS Date), 40.0000, CAST(N'2021-01-28' AS Date), CAST(N'2021-02-28' AS Date), 3, 2)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (5, 5, CAST(N'2021-01-05' AS Date), 500.0000, CAST(N'2021-01-04' AS Date), CAST(N'2021-01-09' AS Date), 5, 1)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (6, 6, CAST(N'2021-02-07' AS Date), 70.0000, CAST(N'2021-01-25' AS Date), CAST(N'2021-02-25' AS Date), 6, 1)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (8, 8, CAST(N'2021-02-08' AS Date), 50.0000, CAST(N'2021-01-29' AS Date), CAST(N'2021-02-28' AS Date), 8, 6)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (9, 9, CAST(N'2021-02-03' AS Date), 100.0000, CAST(N'2021-01-30' AS Date), CAST(N'2021-02-15' AS Date), 9, 2)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (10, 10, CAST(N'2021-02-09' AS Date), 100.0000, CAST(N'2021-01-28' AS Date), CAST(N'2021-02-28' AS Date), 10, 2)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (11, 0, CAST(N'2021-02-07' AS Date), 27.0000, CAST(N'2021-02-06' AS Date), CAST(N'2021-02-09' AS Date), 15, 2)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (16, 15, CAST(N'2021-01-08' AS Date), 15.0000, CAST(N'2021-01-01' AS Date), CAST(N'2021-02-01' AS Date), 5, 1)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (17, 17, CAST(N'2021-02-26' AS Date), 350.0000, CAST(N'2021-02-25' AS Date), CAST(N'2021-03-26' AS Date), 3, 2)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (18, 18, CAST(N'2021-02-27' AS Date), 280.0000, CAST(N'2021-02-25' AS Date), CAST(N'2021-03-27' AS Date), 1, 1)
INSERT [dbo].[DeliveryTable] ([DeliveryId], [ProductId], [DateDelivery], [UnitPrice], [DateOfManufacture], [ShelfLife], [CountDelivery], [UnitId]) VALUES (19, 2, CAST(N'2021-02-25' AS Date), 30.0000, CAST(N'2021-02-10' AS Date), CAST(N'2021-08-21' AS Date), 30, 1)
SET IDENTITY_INSERT [dbo].[DeliveryTable] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductTable] ON 

INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (0, N'Хлеб нарезной', 2, 6)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (1, N'Хлеб', 2, 6)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (2, N'Картофель', 3, 7)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (3, N'Агуша', 11, 14)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (5, N'Наполеон', 4, 1)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (6, N'Карась', 6, 8)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (7, N'Яйца', 7, 9)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (8, N'Кока-кола', 9, 10)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (9, N'Вареная колбаса', 5, 5)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (10, N'Сливочное масло', 8, 11)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (13, N'Молоко', 14, 12)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (15, N'Морковь', 3, 7)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (17, N'Сыр', 14, 15)
INSERT [dbo].[ProductTable] ([ProductId], [ProductName], [CategoryId], [VendorId]) VALUES (18, N'Колбаса молочная', 5, 5)
SET IDENTITY_INSERT [dbo].[ProductTable] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleTable] ON 

INSERT [dbo].[RoleTable] ([RoleId], [NameRole]) VALUES (1, N'Администратор')
INSERT [dbo].[RoleTable] ([RoleId], [NameRole]) VALUES (2, N'Менеджер')
INSERT [dbo].[RoleTable] ([RoleId], [NameRole]) VALUES (3, N'Продавец')
SET IDENTITY_INSERT [dbo].[RoleTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SaleTable] ON 

INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (14, 8, 50, 8, 35, 5, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (15, 11, 27, 1, 36, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (19, 2, 30, 2, 40, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (20, 6, 70, 6, 41, 5, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (21, 8, 50, 1, 42, 0, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (22, 2, 30, 3, 43, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (23, 5, 500, 5, 44, 0, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (25, 3, 40, 3, 46, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (26, 2, 30, 1, 47, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (27, 5, 500, 1, 48, 10, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (28, 6, 70, 1, 49, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (30, 8, 50, 1, 51, 10, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (31, 9, 100, 1, 52, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (32, 11, 27, 1, 53, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (33, 10, 100, 1, 54, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (34, 2, 30, 1, 55, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (35, 6, 70, 1, 56, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (36, 10, 100, 1, 57, 5, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (37, 6, 70, 1, 58, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (38, 5, 500, 1, 59, 5, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (40, 8, 50, 1, 61, 0, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (41, 2, 30, 1, 62, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (43, 10, 100, 1, 64, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (44, 9, 100, 1, 65, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (45, 8, 50, 1, 66, 0, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (46, 2, 30, 1, 67, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (47, 5, 500, 1, 68, 5, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (51, 2, 30, 3, 73, 10, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (52, 8, 50, 4, 73, 5, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (57, 3, 40, 1, 79, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (58, 5, 500, 1, 79, 10, 20)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (59, 2, 30, 1, 80, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (60, 11, 27, 1, 80, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (61, 10, 100, 1, 80, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (62, 9, 100, 1, 81, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (63, 3, 40, 1, 82, 0, 10)
INSERT [dbo].[SaleTable] ([SaleId], [DeliveryId], [PriceWithoutDiscount], [CountSale], [CheckId], [Discount], [VATSale]) VALUES (64, 10, 100, 2, 83, 0, 10)
SET IDENTITY_INSERT [dbo].[SaleTable] OFF
GO
SET IDENTITY_INSERT [dbo].[UnitTable] ON 

INSERT [dbo].[UnitTable] ([UnitId], [NameUnit]) VALUES (1, N'кг')
INSERT [dbo].[UnitTable] ([UnitId], [NameUnit]) VALUES (2, N'шт')
INSERT [dbo].[UnitTable] ([UnitId], [NameUnit]) VALUES (6, N'л')
SET IDENTITY_INSERT [dbo].[UnitTable] OFF
GO
SET IDENTITY_INSERT [dbo].[UserTable] ON 

INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (1, N'admin', N'qwerty123                                         ', 1, N'Владимир Геннадьевич')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (3, N'red1', N'123456789                                         ', 2, N'Александр Владимирович')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (4, N'red2', N'asdfgh122                                         ', 2, N'Виктория Александровна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (9, N'candy_frut', N'c!ndfr!                                           ', 3, N'Людмила Александровна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (10, N'meatOmsk', N'drfrdvrfr34                                       ', 3, N'Денис Иванович')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (11, N'MilkMoskow', N'milkoshop45                                       ', 3, N'Мария Алексеевна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (13, N'breadBest', N'xpxpxpxp2                                         ', 3, N'Ольга Олеговна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (14, N'VegetablesSupply', N'dhjkfkvhjbhj                                      ', 3, N'Сергей Антонович')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (15, N'fishParty', N'kjdfdv45dvf                                       ', 3, N'Алексей Анатольевич')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (16, N'eggMorning', N'goodeggs                                          ', 3, N'Анна Михайловна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (17, N'drinks', N'drinks34                                          ', 3, N'Елена Анатольевна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (18, N'your_joy', N'joy_joy_joy                                       ', 3, N'Виолетта Романовна')
INSERT [dbo].[UserTable] ([UserId], [UserLogin], [UserPassword], [RoleId], [NameUser]) VALUES (21, N'Cost_45', N'sdzxcv                                            ', 3, N'Константин Валерьевич')
SET IDENTITY_INSERT [dbo].[UserTable] OFF
GO
SET IDENTITY_INSERT [dbo].[VATTable] ON 

INSERT [dbo].[VATTable] ([VATId], [Percent]) VALUES (1, 10)
INSERT [dbo].[VATTable] ([VATId], [Percent]) VALUES (2, 20)
SET IDENTITY_INSERT [dbo].[VATTable] OFF
GO
SET IDENTITY_INSERT [dbo].[VendorTable] ON 

INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (1, N'Кэнди Фрут', N'Людмила Александровна', N'89136013349', N'Омск, 10 лет Октября, 109')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (5, N'Омская мясная мануфактура', N'Денис Иванович', N'89859220969', N'Омск, Маяковского, 21')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (6, N'Горячий хлеб', N'Ольга Олеговна', N'89513094099', N'Москва, Ленина, 88')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (7, N'Овощной рай', N'Сергей Антонович', N'89793434327', N'Санкт-Петербург, Декабристов, 37')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (8, N'Fish-party', N'Алексей Анатольевич', N'89414567234', N'Томск, проспект Комарова, 17')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (9, N'Egg-morning', N'Анна Михайловна', N'89511196482', N'Орск, Завертяева, 12')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (10, N'Разливные сидры и напитки', N'Елена Анатольевна', N'89513099439', N'Омск, Энтузиастов, 43')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (11, N'Пищевые жиры', N'Константин Валерьевич', N'89856565653', N'Москва, Нефтезаводская, 6')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (12, N'Milko-маркет', N'Мария Алексеевна', N'89856768649', N'Москва, 22 апреля, 38')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (14, N'Ваша Радость', N'Виолетта Романовна', N'89133289032', N'Рязань, Универсальная, 21')
INSERT [dbo].[VendorTable] ([VendorId], [VendorName], [ContactPerson], [PhonNumber], [Adress]) VALUES (15, N'ООО Зенченко', N'Иван Иванович', N'87774170518', N'Петропавловск, Мира, 108')
SET IDENTITY_INSERT [dbo].[VendorTable] OFF
GO
ALTER TABLE [dbo].[BasketTable]  WITH CHECK ADD  CONSTRAINT [FK_BasketTable_DeliveryTable] FOREIGN KEY([DeliveryId])
REFERENCES [dbo].[DeliveryTable] ([DeliveryId])
GO
ALTER TABLE [dbo].[BasketTable] CHECK CONSTRAINT [FK_BasketTable_DeliveryTable]
GO
ALTER TABLE [dbo].[CategoryTable]  WITH CHECK ADD  CONSTRAINT [FK_CategoryTable_VATTable] FOREIGN KEY([VATId])
REFERENCES [dbo].[VATTable] ([VATId])
GO
ALTER TABLE [dbo].[CategoryTable] CHECK CONSTRAINT [FK_CategoryTable_VATTable]
GO
ALTER TABLE [dbo].[CheckTable]  WITH CHECK ADD  CONSTRAINT [FK_CheckTable_UserTable] FOREIGN KEY([SellerId])
REFERENCES [dbo].[UserTable] ([UserId])
GO
ALTER TABLE [dbo].[CheckTable] CHECK CONSTRAINT [FK_CheckTable_UserTable]
GO
ALTER TABLE [dbo].[DeliveryTable]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryTable_ProductTable] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ProductTable] ([ProductId])
GO
ALTER TABLE [dbo].[DeliveryTable] CHECK CONSTRAINT [FK_DeliveryTable_ProductTable]
GO
ALTER TABLE [dbo].[DeliveryTable]  WITH CHECK ADD  CONSTRAINT [FK_DeliveryTable_UnitTable] FOREIGN KEY([UnitId])
REFERENCES [dbo].[UnitTable] ([UnitId])
GO
ALTER TABLE [dbo].[DeliveryTable] CHECK CONSTRAINT [FK_DeliveryTable_UnitTable]
GO
ALTER TABLE [dbo].[ProductTable]  WITH CHECK ADD  CONSTRAINT [FK_ProductTable_CategoryTable] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[CategoryTable] ([CategoryId])
GO
ALTER TABLE [dbo].[ProductTable] CHECK CONSTRAINT [FK_ProductTable_CategoryTable]
GO
ALTER TABLE [dbo].[ProductTable]  WITH CHECK ADD  CONSTRAINT [FK_ProductTable_VendorTable] FOREIGN KEY([VendorId])
REFERENCES [dbo].[VendorTable] ([VendorId])
GO
ALTER TABLE [dbo].[ProductTable] CHECK CONSTRAINT [FK_ProductTable_VendorTable]
GO
ALTER TABLE [dbo].[SaleTable]  WITH CHECK ADD  CONSTRAINT [FK_SaleTable_CheckTable] FOREIGN KEY([CheckId])
REFERENCES [dbo].[CheckTable] ([CheckId])
GO
ALTER TABLE [dbo].[SaleTable] CHECK CONSTRAINT [FK_SaleTable_CheckTable]
GO
ALTER TABLE [dbo].[SaleTable]  WITH CHECK ADD  CONSTRAINT [FK_SaleTable_DeliveryTable] FOREIGN KEY([DeliveryId])
REFERENCES [dbo].[DeliveryTable] ([DeliveryId])
GO
ALTER TABLE [dbo].[SaleTable] CHECK CONSTRAINT [FK_SaleTable_DeliveryTable]
GO
ALTER TABLE [dbo].[UserTable]  WITH CHECK ADD  CONSTRAINT [FK_UserTable_RoleTable] FOREIGN KEY([RoleId])
REFERENCES [dbo].[RoleTable] ([RoleId])
GO
ALTER TABLE [dbo].[UserTable] CHECK CONSTRAINT [FK_UserTable_RoleTable]
GO
USE [master]
GO
ALTER DATABASE [dbProductShop] SET  READ_WRITE 
GO
