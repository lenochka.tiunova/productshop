﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class unitForm : Form
    {
        public unitForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbNameUnit.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    UnitTable unitTable = new UnitTable
                    {
                        NameUnit = tbNameUnit.Text
                    };
                    db.UnitTable.Add(unitTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var unitTable = db.UnitTable.FirstOrDefault(n => n.UnitId == index);
                    unitTable.NameUnit = tbNameUnit.Text;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните поле.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void unitForm_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование")
            {
                tbNameUnit.Text = db.UnitTable.FirstOrDefault(n => n.UnitId == index).NameUnit;
            }
        }
    }
}
