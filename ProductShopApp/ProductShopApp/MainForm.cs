﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using Font = System.Drawing.Font;

namespace ProductShopApp
{
    public partial class mainForm : Form
    {
        public mainForm(string role)
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            saveWord.Filter = "Документ word|*.docx|Документ word 97-2003|*.doc|Текст в формате RTF|*.rtf|XML-документ Word 2003|*.xml";
            saveExcel.Filter = "Книга Excel|*.xlsx|Веб-страница|*.html";
            if (role != "Администратор")
            {
                SharingOfOpportunities();
            }
        }

        //Подключение базы данных
        dbProductShopEntities db = new dbProductShopEntities();

        string table = "";

        public void SharingOfOpportunities()
        {
            miFilter.Visible = false;
            miExport.Visible = false;
            miReports.Visible = false;
            miEditMenu.Visible = false;
            miRequests.Visible = false;
            miCategory.Visible = false;
            miProducts.Visible = false;
            miVAT.Visible = false;
            miVendors.Visible = false;
            miUsers.Visible = false;
            miRoles.Visible = false;
            miSales.Visible = false;
            miUnit.Visible = false;
            miCheck.Visible = false;
        }

        private void miCategory_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "CategoryTable";
            //Создание запроса на выборку Наименования категории и Кода НДС
            var category = db.CategoryTable.Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
            //Вывод Наименования категории и Кода НДС
            dgvProductShop.DataSource = category;
            //Скрытие столбца с идентификатором категории
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Название категории";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Ндс (%)";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            //Выравнивание текста второй колонки по центру
            dgvProductShop.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miProducts_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "ProductTable";
            //Создание запроса на выборку Наименования продукта, категории и поставщика
            var product = db.ProductTable.Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
            //Вывод Наименования продукта, категории и поставщика
            dgvProductShop.DataSource = product;
            //Скрытие столбца с идентификатором продукта
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Наименование категории";
            //Добавление заголовка третьего столбца
            dgvProductShop.Columns[3].HeaderText = "Поставщик";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miVAT_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = false;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "VATTable";
            //Создание запроса на выборку процента НДС
            var vat = db.VATTable.Select(n => new { n.VATId, n.Percent }).ToList();
            //Вывод процента НДС
            dgvProductShop.DataSource = vat;
            //Скрытие столбца с идентификатором ставки НДС
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Процент";
            //Автоматическое изменение ширины столбца
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовка по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовка на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            //Выравнивание текста колонки по центру
            dgvProductShop.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miVendors_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "VendorTable";
            //Создание запроса на выборку Поставщика, контактного лица, номера телефона и адреса
            var vendors = db.VendorTable.Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
            //Вывод Поставщика, контактного лица, номера телефона и адреса
            dgvProductShop.DataSource = vendors;
            //Скрытие столбца с идентификатором поставщика
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Поставщик";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Контактное лицо";
            //Добавление заголовка третьего столбца
            dgvProductShop.Columns[3].HeaderText = "Номер телефона";
            //Добавление заголовка четвертого столбца
            dgvProductShop.Columns[4].HeaderText = "Адрес";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miUsers_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "UserTable";
            //Создание запроса на выборку Логина, пароля, роли и имени пользователя
            var users = db.UserTable.Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
            //Вывод Логина, пароля, роли и имени пользователя
            dgvProductShop.DataSource = users;
            //Скрытие столбца с идентификатором пользователя
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Логин";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Пароль";
            //Добавление заголовка третьего столбца
            dgvProductShop.Columns[3].HeaderText = "Роль";
            //Добавление заголовка четвертого столбца
            dgvProductShop.Columns[4].HeaderText = "Имя пользователя";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miRoles_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = false;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "RoleTable";
            //Создание запроса на выборку Наименования роли
            var role = db.RoleTable.Select(n => new { n.RoleId, n.NameRole }).ToList();
            //Вывод Наименования роли
            dgvProductShop.DataSource = role;
            //Скрытие столбца с идентификатором роли
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Роль";
            //Автоматическое изменение ширины столбца
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовка по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовка на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            //Выравнивание текста колонки по центру
            dgvProductShop.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miDelivery_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "DeliveryTable";
            //Создание запроса на выборку Наименования продукта, даты доставки, цены за единицу товара, даты производства товара, срока годности товара, количества, единицы измерения
            var delivery = db.DeliveryTable.Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
            //Вывод Наименования продукта, даты доставки, цены за единицу товара, даты производства товара, срока годности товара, количества, единицы измерения
            dgvProductShop.DataSource = delivery;
            //Скрытие столбца с идентификатором доставки
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Наименование";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Дата поставки";
            //Добавление заголовка третьего столбца
            dgvProductShop.Columns[3].HeaderText = "Цена за единицу";
            //Добавление заголовка четвертого столбца
            dgvProductShop.Columns[4].HeaderText = "Дата производства";
            //Добавление заголовка пятого столбца
            dgvProductShop.Columns[5].HeaderText = "Срок годности";
            //Добавление заголовка шестого столбца
            dgvProductShop.Columns[6].HeaderText = "Количество";
            //Добавление заголовка седьмого столбца
            dgvProductShop.Columns[7].HeaderText = "Единица измерения";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miBasket_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = false;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "BasketTable";
            //Создание запроса на выборку Наименования товара и количества
            var basket = db.BasketTable.Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
            //Вывод Наименования товара и количества
            dgvProductShop.DataSource = basket;
            //Скрытие столбца с идентификатором корзины
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Наименование товара";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Количество";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            //Выравнивание текста второй колонки по центру
            dgvProductShop.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miUnit_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = false;
            pnlFilter.Visible = false;
            cc.Enabled = true;
            miEdit.Enabled = true;
            table = "UnitTable";
            //Создание запроса на выборку Единицы измерения
            var unit = db.UnitTable.Select(n => new { n.UnitId, n.NameUnit }).ToList();
            //Вывод Единицы измерения
            dgvProductShop.DataSource = unit;
            //Скрытие столбца с идентификатором Единицы измерения
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Единица измерения";
            //Автоматическое изменение ширины столбца
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовка по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовка на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            //Выравнивание текста колонки по центру
            dgvProductShop.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miSales_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = false;
            miEdit.Enabled = false;
            table = "SaleTable";
            //Создание запроса на выборку Наименования продукта, цены за без скидки, количества, даты проверки, скидки, НДС продажи
            var sale = db.SaleTable.Select(n => new { n.SaleId, n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.CheckTable.DateCheck, n.Discount, n.VATSale }).ToList();
            //Вывод Наименования продукта, цены за без скидки, количества, даты проверки, скидки, НДС продажи
            dgvProductShop.DataSource = sale;
            //Скрытие столбца с идентификатором скидки
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Цена без скидки";
            //Добавление заголовка третьего столбца
            dgvProductShop.Columns[3].HeaderText = "Количество";
            //Добавление заголовка четвертого столбца
            dgvProductShop.Columns[4].HeaderText = "Дата продажи";
            //Добавление заголовка пятого столбца
            dgvProductShop.Columns[5].HeaderText = "Скидка (%)";
            //Добавление заголовка шестого столбца
            dgvProductShop.Columns[6].HeaderText = "НДС продажи (%)";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miCheck_Click(object sender, EventArgs e)
        {
            miFilter.Enabled = true;
            pnlFilter.Visible = false;
            cc.Enabled = false;
            miEdit.Enabled = false;
            table = "CheckTable";
            //Создание запроса на выборку Даты проверки и имени продавца
            var check = db.CheckTable.Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
            //Вывод Даты проверки и имени продавца
            dgvProductShop.DataSource = check;
            //Скрытие столбца с идентификатором проверки
            dgvProductShop.Columns[0].Visible = false;
            //Добавление заголовка первого столбца
            dgvProductShop.Columns[1].HeaderText = "Дата";
            //Добавление заголовка второго столбца
            dgvProductShop.Columns[2].HeaderText = "Имя продавца";
            //Автоматическое изменение ширины столбцов
            dgvProductShop.AutoResizeColumns();
            //Выравнивание заголовков по центру
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Изменение начертания заголовков на полужирный
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            //Выравнивание текста первой колонки по центру
            dgvProductShop.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Выравнивание текста второй колонки по центру
            dgvProductShop.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery1_Click(object sender, EventArgs e)
        {
            //создание запроса
            var query = db.CategoryTable.Where(m => m.VATTable.Percent == 20).Select(n => new { n.CategoryName }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Название категории";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery2_Click(object sender, EventArgs e)
        {
            //создание запроса
            var query = db.CategoryTable.Where(m => m.VATTable.Percent == 10).Select(n => new { n.CategoryName }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Название категории";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery3_Click(object sender, EventArgs e)
        {
            //создание запроса
            var query = db.UserTable.Where(m => m.RoleTable.NameRole == "Продавец").Select(n => new { n.NameUser }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Имена сотрудников";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery4_Click(object sender, EventArgs e)
        {
            //создание запроса
            var query = db.SaleTable.Where(m => m.Discount != 0).Select(n => new { n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.Discount }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Наименование товара";
            dgvProductShop.Columns[0].HeaderText = "Цена без скидки";
            dgvProductShop.Columns[0].HeaderText = "Количество";
            dgvProductShop.Columns[0].HeaderText = "Скидка (%)";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery5_Click(object sender, EventArgs e)
        {
            //создание запроса
            var query = db.ProductTable.Where(m => m.CategoryTable.VATTable.Percent == 10).Select(n => new { n.ProductName }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Название продукта";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery6_Click(object sender, EventArgs e)
        {
            //создание запроса
            var query = db.ProductTable.Where(m => m.CategoryTable.VATTable.Percent == 20).Select(n => new { n.ProductName }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Название продукта";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void SearchString(string textSearch)
        {
            bool check = true;
            if (table == "CategoryTable")
            {
                var search = db.CategoryTable.Where(n => n.CategoryName.Contains(textSearch))
                    .Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Название категории";
                dgvProductShop.Columns[2].HeaderText = "НДС (%)";
            }
            else if (table == "ProductTable")
            {
                var search = db.ProductTable
                    .Where(n => n.ProductName.Contains(textSearch) || n.CategoryTable.CategoryName.Contains(textSearch) || n.VendorTable.VendorName.Contains(textSearch))
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Наименование категории";
                dgvProductShop.Columns[3].HeaderText = "Поставщик";
            }
            else if (table == "VendorTable")
            {
                var search = db.VendorTable
                    .Where(n => n.VendorName.Contains(textSearch) || n.ContactPerson.Contains(textSearch) || n.Adress.Contains(textSearch))
                    .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Поставщик";
                dgvProductShop.Columns[2].HeaderText = "Контактное лицо";
                dgvProductShop.Columns[3].HeaderText = "Номер телефона";
                dgvProductShop.Columns[4].HeaderText = "Адрес";
            }
            else if (table == "UserTable")
            {
                var search = db.UserTable
                    .Where(n => n.UserLogin.Contains(textSearch) || n.UserPassword.Contains(textSearch) || n.RoleTable.NameRole.Contains(textSearch) || n.NameUser.Contains(textSearch))
                    .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Логин";
                dgvProductShop.Columns[2].HeaderText = "Пароль";
                dgvProductShop.Columns[3].HeaderText = "Роль";
                dgvProductShop.Columns[4].HeaderText = "Имя пользователя";
            }
            else if (table == "RoleTable")
            {
                var search = db.RoleTable.Where(n => n.NameRole.Contains(textSearch))
                    .Select(n => new { n.RoleId, n.NameRole }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Роль";
            }
            else if (table == "DeliveryTable")
            {
                var search = db.DeliveryTable.Where(n => n.ProductTable.ProductName.Contains(textSearch) || n.UnitTable.NameUnit.Contains(textSearch))
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Наименование";
                dgvProductShop.Columns[2].HeaderText = "Дата поставки";
                dgvProductShop.Columns[3].HeaderText = "Цена за единицу";
                dgvProductShop.Columns[4].HeaderText = "Дата производства";
                dgvProductShop.Columns[5].HeaderText = "Срок годности";
                dgvProductShop.Columns[6].HeaderText = "Количество";
                dgvProductShop.Columns[7].HeaderText = "Единица измерения";
            }
            else if (table == "BasketTable")
            {
                var search = db.BasketTable.Where(n => n.DeliveryTable.ProductTable.ProductName.Contains(textSearch))
                    .Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Наименование товара";
                dgvProductShop.Columns[2].HeaderText = "Количество";
            }
            else if (table == "UnitTable")
            {
                var search = db.UnitTable.Where(n => n.NameUnit.Contains(textSearch))
                    .Select(n => new { n.UnitId, n.NameUnit }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Единица измерения";
            }
            else if (table == "SaleTable")
            {
                var search = db.SaleTable.Where(n => n.DeliveryTable.ProductTable.ProductName.Contains(textSearch))
                    .Select(n => new { n.SaleId, n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.CheckTable.DateCheck, n.Discount, n.VATSale }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Цена без скидки";
                dgvProductShop.Columns[3].HeaderText = "Количество";
                dgvProductShop.Columns[4].HeaderText = "Дата проверки";
                dgvProductShop.Columns[5].HeaderText = "Скидка (%)";
                dgvProductShop.Columns[6].HeaderText = "НДС продажи (%)";
            }
            else if (table == "CheckTable")
            {
                var search = db.CheckTable.Where(n => n.UserTable.NameUser.Contains(textSearch))
                    .Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
                dgvProductShop.DataSource = search;
                dgvProductShop.Columns[1].HeaderText = "Дата";
                dgvProductShop.Columns[2].HeaderText = "Имя продавца";
            }
            else
            {
                MessageBox.Show("Выберите таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                check = false;
            }

            if (check)
            {
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.AutoResizeColumns();
                dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string textSearch = tbSearch.Text;
            SearchString(textSearch);
        }

        private void miReports_Click(object sender, EventArgs e)
        {
            reportForm reportForm = new reportForm();
            reportForm.Show();
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            if (table == "CategoryTable")
            {
                CategoryForm categoryForm = new CategoryForm();
                categoryForm.Text = "Добавление";
                categoryForm.ShowDialog();
                miCategory_Click(null, null);
            }
            else if (table == "UnitTable")
            {
                unitForm unitForm = new unitForm();
                unitForm.Text = "Добавление";
                unitForm.ShowDialog();
                miUnit_Click(null, null);
            }
            else if (table == "ProductTable")
            {
                ProductForm productForm = new ProductForm();
                productForm.Text = "Добавление";
                productForm.ShowDialog();
                miProducts_Click(null, null);
            }
            else if (table == "VATTable")
            {
                VATForm VATForm = new VATForm();
                VATForm.Text = "Добавление";
                VATForm.ShowDialog();
                miVAT_Click(null, null);
            }
            else if (table == "VendorTable")
            {
                VendorForm vendorForm = new VendorForm();
                vendorForm.Text = "Добавление";
                vendorForm.ShowDialog();
                miVendors_Click(null, null);
            }
            else if (table == "UserTable")
            {
                UserForm userForm = new UserForm();
                userForm.Text = "Добавление";
                userForm.ShowDialog();
                miUsers_Click(null, null);
            }
            else if (table == "RoleTable")
            {
                RoleForm roleForm = new RoleForm();
                roleForm.Text = "Добавление";
                roleForm.ShowDialog();
                miRoles_Click(null, null);
            }
            else if (table == "DeliveryTable")
            {
                DeliveryForm deliveryForm = new DeliveryForm();
                deliveryForm.Text = "Добавление";
                deliveryForm.ShowDialog();
                miDelivery_Click(null, null);
            }
            else if (table == "BasketTable")
            {
                BasketForm basketForm = new BasketForm();
                basketForm.Text = "Добавление";
                basketForm.ShowDialog();
                miBasket_Click(null, null);
            }
            else
            {
                MessageBox.Show("Выберите таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (table == "CategoryTable")
                {
                    CategoryForm categoryForm = new CategoryForm();
                    categoryForm.Text = "Редактирование";
                    categoryForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    categoryForm.ShowDialog();
                    miCategory_Click(null, null);
                }
                else if (table == "UnitTable")
                {
                    unitForm unitForm = new unitForm();
                    unitForm.Text = "Редактирование";
                    unitForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    unitForm.ShowDialog();
                    miUnit_Click(null, null);
                }
                else if (table == "ProductTable")
                {
                    ProductForm productForm = new ProductForm();
                    productForm.Text = "Редактирование";
                    productForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    productForm.ShowDialog();
                    miProducts_Click(null, null);
                }
                else if (table == "VATTable")
                {
                    VATForm VATForm = new VATForm();
                    VATForm.Text = "Редактирование";
                    VATForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    VATForm.ShowDialog();
                    miVAT_Click(null, null);
                }
                else if (table == "VendorTable")
                {
                    VendorForm vendorForm = new VendorForm();
                    vendorForm.Text = "Редактирование";
                    vendorForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    vendorForm.ShowDialog();
                    miVendors_Click(null, null);
                }
                else if (table == "UserTable")
                {
                    UserForm userForm = new UserForm();
                    userForm.Text = "Редактирование";
                    userForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    userForm.ShowDialog();
                    miUsers_Click(null, null);
                }
                else if (table == "RoleTable")
                {
                    RoleForm roleForm = new RoleForm();
                    roleForm.Text = "Редактирование";
                    roleForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    roleForm.ShowDialog();
                    miRoles_Click(null, null);
                }
                else if (table == "DeliveryTable")
                {
                    DeliveryForm deliveryForm = new DeliveryForm();
                    deliveryForm.Text = "Редактирование";
                    deliveryForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    deliveryForm.ShowDialog();
                    miDelivery_Click(null, null);
                }
                else if (table == "BasketTable")
                {
                    BasketForm basketForm = new BasketForm();
                    basketForm.Text = "Редактирование";
                    basketForm.index = int.Parse(dgvProductShop.Rows[dgvProductShop.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    basketForm.ShowDialog();
                    miBasket_Click(null, null);
                }
                else
                {
                    MessageBox.Show("Выберите таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            if(dgvProductShop.CurrentRow != null)
            {
                if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    var index = int.Parse(dgvProductShop[0, dgvProductShop.CurrentRow.Index].Value.ToString());
                    if (table == "CategoryTable")
                    {
                        var category = db.CategoryTable.FirstOrDefault(n => n.CategoryId == index);
                        var product = db.ProductTable.Where(n => n.CategoryId == index).ToList();
                        var delivery = db.DeliveryTable.Where(n => n.ProductTable.CategoryId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.DeliveryTable.ProductTable.CategoryId == index).ToList();
                        var basket = db.BasketTable.Where(n => n.DeliveryTable.ProductTable.CategoryId == index).ToList();
                        db.BasketTable.RemoveRange(basket);
                        db.ProductTable.RemoveRange(product);
                        db.DeliveryTable.RemoveRange(delivery);
                        db.SaleTable.RemoveRange(sale);
                        db.CategoryTable.Remove(category);
                        db.SaveChanges();
                        miCategory_Click(null, null);
                    }
                    else if (table == "BasketTable")
                    {
                        var basket = db.BasketTable.FirstOrDefault(n => n.BasketId == index);
                        db.BasketTable.Remove(basket);
                        db.SaveChanges();
                        miBasket_Click(null, null);
                    }
                    else if (table == "CheckTable")
                    {
                        var check = db.CheckTable.FirstOrDefault(n => n.CheckId == index);
                        var sale = db.SaleTable.Where(n => n.CheckTable.CheckId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.CheckTable.Remove(check);
                        db.SaveChanges();
                        miCheck_Click(null, null);
                    }
                    else if (table == "DeliveryTable")
                    {
                        var delivery = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index);
                        var sale = db.SaleTable.Where(n => n.DeliveryId == index).ToList();
                        var basket = db.BasketTable.Where(n => n.DeliveryId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.BasketTable.RemoveRange(basket);
                        db.DeliveryTable.Remove(delivery);
                        db.SaveChanges();
                        miDelivery_Click(null, null);
                    }
                    else if (table == "ProductTable")
                    {
                        var product = db.ProductTable.FirstOrDefault(n => n.ProductId == index);
                        var delivery = db.DeliveryTable.Where(n => n.ProductId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.DeliveryTable.ProductTable.ProductId == index).ToList();
                        var basket = db.BasketTable.Where(n => n.DeliveryTable.ProductTable.ProductId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.BasketTable.RemoveRange(basket);
                        db.DeliveryTable.RemoveRange(delivery);
                        db.ProductTable.Remove(product);
                        db.SaveChanges();
                        miProducts_Click(null, null);
                    }
                    else if (table == "RoleTable")
                    {
                        var role = db.RoleTable.FirstOrDefault(n => n.RoleId == index);
                        var user = db.UserTable.Where(n => n.RoleId == index).ToList();
                        var check = db.CheckTable.Where(n => n.UserTable.RoleId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.CheckTable.UserTable.RoleId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.CheckTable.RemoveRange(check);
                        db.UserTable.RemoveRange(user);
                        db.RoleTable.Remove(role);
                        db.SaveChanges();
                        miRoles_Click(null, null);
                    }
                    else if (table == "SaleTable")
                    {
                        var sale = db.SaleTable.FirstOrDefault(n => n.SaleId == index);
                        db.SaleTable.Remove(sale);
                        db.SaveChanges();
                        miSales_Click(null, null);
                    }
                    else if (table == "UnitTable")
                    {
                        var unit = db.UnitTable.Where(n => n.UnitId == index).ToList();
                        var delivery = db.DeliveryTable.Where(n => n.UnitId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.DeliveryTable.UnitId == index).ToList();
                        var basket = db.BasketTable.Where(n => n.DeliveryTable.UnitId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.BasketTable.RemoveRange(basket);
                        db.DeliveryTable.RemoveRange(delivery);
                        db.UnitTable.RemoveRange(unit);
                        db.SaveChanges();
                        miUnit_Click(null, null);
                    }
                    else if (table == "UserTable")
                    {
                        var user = db.UserTable.Where(n => n.UserId == index).ToList();
                        var check = db.CheckTable.Where(n => n.SellerId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.CheckTable.SellerId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.CheckTable.RemoveRange(check);
                        db.UserTable.RemoveRange(user);
                        db.SaveChanges();
                        miUsers_Click(null, null);
                    }
                    else if (table == "VATTable")
                    {
                        var vat = db.VATTable.FirstOrDefault(n => n.VATId == index);
                        var category = db.CategoryTable.Where(n => n.VATId == index).ToList();
                        var product = db.ProductTable.Where(n => n.CategoryTable.VATId == index).ToList();
                        var delivery = db.DeliveryTable.Where(n => n.ProductTable.CategoryTable.VATId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.DeliveryTable.ProductTable.CategoryTable.VATId == index).ToList();
                        db.DeliveryTable.RemoveRange(delivery);
                        db.SaleTable.RemoveRange(sale);
                        db.ProductTable.RemoveRange(product);
                        db.CategoryTable.RemoveRange(category);
                        db.VATTable.Remove(vat);
                        db.SaveChanges();
                        miVAT_Click(null, null);
                    }
                    else if (table == "VendorTable")
                    {
                        var vendor = db.VendorTable.FirstOrDefault(n => n.VendorId == index);
                        var product = db.ProductTable.Where(n => n.VendorId == index).ToList();
                        var delivery = db.DeliveryTable.Where(n => n.ProductTable.VendorId == index).ToList();
                        var sale = db.SaleTable.Where(n => n.DeliveryTable.ProductTable.VendorId == index).ToList();
                        var basket = db.BasketTable.Where(n => n.DeliveryTable.ProductTable.VendorId == index).ToList();
                        db.SaleTable.RemoveRange(sale);
                        db.BasketTable.RemoveRange(basket);
                        db.DeliveryTable.RemoveRange(delivery);
                        db.ProductTable.RemoveRange(product);
                        db.VendorTable.Remove(vendor);
                        db.SaveChanges();
                        miVendors_Click(null, null);
                    }
                }
            }
            else
            {
                MessageBox.Show("Выберите запись для удаления", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void miBasketMenu_Click(object sender, EventArgs e)
        {
            Basket basket = new Basket();
            basket.ShowDialog();
            if (table == "SaleTable")
            {
                miSales_Click(null, null);
            }
        }

        bool categorySortName = true;
        bool categoryVAT = true;
        bool basketProduct = true;
        bool basketCount = true;
        bool dateCheck = true;
        bool sellerName = true;
        bool roleName = true;
        bool deliveryProduct = true;
        bool dateDelivery = true;
        bool unitPrice = true;
        bool dateOfManufacture = true;
        bool dateShelfLife = true;
        bool deliveryCount = true;
        bool deliveryUnit = true;
        bool nameProduct = true;
        bool productCategory = true;
        bool productVendor = true;
        bool saleProductName = true;
        bool salePrice = true;
        bool saleCount = true;
        bool saleDateCheck = true;
        bool discountSale = true;
        bool saleVAT = true;
        bool unitFromUnitTable = true;
        bool vatPercent = true;
        bool login = true;
        bool password = true;
        bool userRole = true;
        bool name = true;
        bool vendorName = true;
        bool nameContactPerson = true;
        bool vendorPhoneNumber = true;
        bool vendorAdress = true;



        private void dgvProductShop_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (table == "CategoryTable")
            {
                var category = db.CategoryTable.Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (categorySortName == true)
                    {
                        category = db.CategoryTable.OrderBy(m => m.CategoryName)
                            .Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                        categorySortName = false;
                    }
                    else
                    {
                        category = db.CategoryTable.OrderByDescending(m => m.CategoryName)
                            .Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                        categorySortName = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (categoryVAT == true)
                    {
                        category = db.CategoryTable.OrderBy(m => m.VATTable.Percent)
                            .Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                        categoryVAT = false;
                    }
                    else
                    {
                        category = db.CategoryTable.OrderByDescending(m => m.VATTable.Percent)
                            .Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                        categoryVAT = true;
                    }
                }
                dgvProductShop.DataSource = category;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Название категории";
                dgvProductShop.Columns[2].HeaderText = "Ндс (%)";
            }
            else if (table == "BasketTable")
            {
                var basket = db.BasketTable.Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (basketProduct == true)
                    {
                        basket = db.BasketTable.OrderBy(m => m.DeliveryTable.ProductTable.ProductName)
                            .Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
                        basketProduct = false;
                    }
                    else
                    {
                        basket = db.BasketTable.OrderByDescending(m => m.DeliveryTable.ProductTable.ProductName)
                            .Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
                        basketProduct = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (basketCount == true)
                    {
                        basket = db.BasketTable.OrderBy(m => m.CountBasket)
                            .Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
                        basketCount = false;
                    }
                    else
                    {
                        basket = db.BasketTable.OrderByDescending(m => m.CountBasket)
                            .Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
                        basketCount = true;
                    }
                }
                dgvProductShop.DataSource = basket;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование товара";
                dgvProductShop.Columns[2].HeaderText = "Количество";
            }
            else if (table == "CheckTable")
            {
                var check = db.CheckTable.Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (dateCheck == true)
                    {
                        check = db.CheckTable.OrderBy(m => m.DateCheck)
                            .Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
                        dateCheck = false;
                    }
                    else
                    {
                        check = db.CheckTable.OrderByDescending(m => m.DateCheck)
                            .Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
                        dateCheck = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (sellerName == true)
                    {
                        check = db.CheckTable.OrderBy(m => m.UserTable.NameUser)
                            .Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
                        sellerName = false;
                    }
                    else
                    {
                        check = db.CheckTable.OrderByDescending(m => m.UserTable.NameUser)
                            .Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser }).ToList();
                        sellerName = true;
                    }
                }
                dgvProductShop.DataSource = check;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Дата";
                dgvProductShop.Columns[2].HeaderText = "Имя продавца";
            }
            else if (table == "DeliveryTable")
            {
                var delivery = db.DeliveryTable
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (deliveryProduct == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.ProductTable.ProductName)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        deliveryProduct = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.ProductTable.ProductName)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        deliveryProduct = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (dateDelivery == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.DateDelivery)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        dateDelivery = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.DateDelivery)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        dateDelivery = true;
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (unitPrice == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.UnitPrice)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        unitPrice = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.UnitPrice)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        unitPrice = true;
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (dateOfManufacture == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.DateOfManufacture)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        dateOfManufacture = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.DateOfManufacture)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        dateOfManufacture = true;
                    }
                }
                if (e.ColumnIndex == 5)
                {
                    if (dateShelfLife == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.ShelfLife)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        dateShelfLife = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.ShelfLife)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        dateShelfLife = true;
                    }
                }
                if (e.ColumnIndex == 6)
                {
                    if (deliveryCount == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.CountDelivery)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        deliveryCount = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.CountDelivery)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        deliveryCount = true;
                    }
                }
                if (e.ColumnIndex == 7)
                {
                    if (deliveryUnit == true)
                    {
                        delivery = db.DeliveryTable.OrderBy(m => m.UnitTable.NameUnit)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        deliveryUnit = false;
                    }
                    else
                    {
                        delivery = db.DeliveryTable.OrderByDescending(m => m.UnitTable.NameUnit)
                    .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                        deliveryUnit = true;
                    }
                }
                dgvProductShop.DataSource = delivery;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование";
                dgvProductShop.Columns[2].HeaderText = "Дата поставки";
                dgvProductShop.Columns[3].HeaderText = "Цена за единицу";
                dgvProductShop.Columns[4].HeaderText = "Дата производства";
                dgvProductShop.Columns[5].HeaderText = "Срок годности";
                dgvProductShop.Columns[6].HeaderText = "Количество";
                dgvProductShop.Columns[7].HeaderText = "Единица измерения";
            }
            else if (table == "ProductTable")
            {
                var product = db.ProductTable
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (nameProduct == true)
                    {
                        product = db.ProductTable.OrderBy(m => m.ProductName)
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                        nameProduct = false;
                    }
                    else
                    {
                        product = db.ProductTable.OrderByDescending(m => m.ProductName)
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                        nameProduct = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (productCategory == true)
                    {
                        product = db.ProductTable.OrderBy(m => m.CategoryTable.CategoryName)
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                        productCategory = false;
                    }
                    else
                    {
                        product = db.ProductTable.OrderByDescending(m => m.CategoryTable.CategoryName)
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                        productCategory = true;
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (productVendor == true)
                    {
                        product = db.ProductTable.OrderBy(m => m.VendorTable.VendorName)
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                        productVendor = false;
                    }
                    else
                    {
                        product = db.ProductTable.OrderByDescending(m => m.VendorTable.VendorName)
                    .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                        productVendor = true;
                    }
                }
                dgvProductShop.DataSource = product;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Наименование категории";
                dgvProductShop.Columns[3].HeaderText = "Поставщик";
            }
            else if (table == "RoleTable")
            {
                var role = db.RoleTable.Select(n => new { n.RoleId, n.NameRole }).ToList();
                if (roleName == true)
                {
                    role = db.RoleTable.OrderBy(m => m.NameRole).Select(n => new { n.RoleId, n.NameRole }).ToList();
                    roleName = false;
                }
                else
                {
                    role = db.RoleTable.OrderByDescending(m => m.NameRole).Select(n => new { n.RoleId, n.NameRole }).ToList();
                    roleName = true;
                }
                dgvProductShop.DataSource = role;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Роль";
            }
            else if (table == "SaleTable")
            {
                var sale = db.SaleTable
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (saleProductName == true)
                    {
                        sale = db.SaleTable.OrderBy(m => m.DeliveryTable.ProductTable.ProductName)
                            .Select(n => new
                            {
                                n.SaleId,
                                n.DeliveryTable.ProductTable.ProductName,
                                n.PriceWithoutDiscount,
                                n.CountSale,
                                n.CheckTable.DateCheck,
                                n.Discount,
                                n.VATSale
                            }).ToList();
                        saleProductName = false;
                    }
                    else
                    {
                        sale = db.SaleTable.OrderByDescending(m => m.DeliveryTable.ProductTable.ProductName)
                            .Select(n => new
                            {
                                n.SaleId,
                                n.DeliveryTable.ProductTable.ProductName,
                                n.PriceWithoutDiscount,
                                n.CountSale,
                                n.CheckTable.DateCheck,
                                n.Discount,
                                n.VATSale
                            }).ToList();
                        saleProductName = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (salePrice == true)
                    {
                        sale = db.SaleTable.OrderBy(m => m.PriceWithoutDiscount)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        salePrice = false;
                    }
                    else
                    {
                        sale = db.SaleTable.OrderByDescending(m => m.PriceWithoutDiscount)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        salePrice = true;
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (saleCount == true)
                    {
                        sale = db.SaleTable.OrderBy(m => m.CountSale)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        saleCount = false;
                    }
                    else
                    {
                        sale = db.SaleTable.OrderByDescending(m => m.CountSale)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        saleCount = true;
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (saleDateCheck == true)
                    {
                        sale = db.SaleTable.OrderBy(m => m.CheckTable.DateCheck)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        saleDateCheck = false;
                    }
                    else
                    {
                        sale = db.SaleTable.OrderByDescending(m => m.CheckTable.DateCheck)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        saleDateCheck = true;
                    }
                }
                if (e.ColumnIndex == 5)
                {
                    if (discountSale == true)
                    {
                        sale = db.SaleTable.OrderBy(m => m.Discount)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        discountSale = false;
                    }
                    else
                    {
                        sale = db.SaleTable.OrderByDescending(m => m.Discount)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        discountSale = true;
                    }
                }
                if (e.ColumnIndex == 6)
                {
                    if (saleVAT == true)
                    {
                        sale = db.SaleTable.OrderBy(m => m.VATSale)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        saleVAT = false;
                    }
                    else
                    {
                        sale = db.SaleTable.OrderByDescending(m => m.VATSale)
                    .Select(n => new
                    {
                        n.SaleId,
                        n.DeliveryTable.ProductTable.ProductName,
                        n.PriceWithoutDiscount,
                        n.CountSale,
                        n.CheckTable.DateCheck,
                        n.Discount,
                        n.VATSale
                    }).ToList();
                        saleVAT = true;
                    }
                }
                dgvProductShop.DataSource = sale;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Цена без скидки";
                dgvProductShop.Columns[3].HeaderText = "Количество";
                dgvProductShop.Columns[4].HeaderText = "Дата проверки";
                dgvProductShop.Columns[5].HeaderText = "Скидка (%)";
                dgvProductShop.Columns[6].HeaderText = "НДС продажи (%)";
            }
            else if (table == "UnitTable")
            {
                var unit = db.UnitTable.Select(n => new { n.UnitId, n.NameUnit }).ToList();
                if (unitFromUnitTable == true)
                {
                    unit = db.UnitTable.OrderBy(m => m.NameUnit).Select(n => new { n.UnitId, n.NameUnit }).ToList();
                    unitFromUnitTable = false;
                }
                else
                {
                    unit = db.UnitTable.OrderByDescending(m => m.NameUnit).Select(n => new { n.UnitId, n.NameUnit }).ToList();
                    unitFromUnitTable = true;
                }
                dgvProductShop.DataSource = unit;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Единица измерения";
            }
            else if (table == "UserTable")
            {
                var user = db.UserTable
                    .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (login == true)
                    {
                        user = db.UserTable.OrderBy(m => m.UserLogin)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        login = false;
                    }
                    else
                    {
                        user = db.UserTable.OrderByDescending(m => m.UserLogin)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        login = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (password == true)
                    {
                        user = db.UserTable.OrderBy(m => m.UserPassword)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        password = false;
                    }
                    else
                    {
                        user = db.UserTable.OrderByDescending(m => m.UserPassword)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        password = true;
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (userRole == true)
                    {
                        user = db.UserTable.OrderBy(m => m.RoleTable.NameRole)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        userRole = false;
                    }
                    else
                    {
                        user = db.UserTable.OrderByDescending(m => m.RoleTable.NameRole)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        userRole = true;
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (name == true)
                    {
                        user = db.UserTable.OrderBy(m => m.NameUser)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        name = false;
                    }
                    else
                    {
                        user = db.UserTable.OrderByDescending(m => m.NameUser)
                            .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                        name = true;
                    }
                }
                dgvProductShop.DataSource = user;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Логин";
                dgvProductShop.Columns[2].HeaderText = "Пароль";
                dgvProductShop.Columns[3].HeaderText = "Роль";
                dgvProductShop.Columns[4].HeaderText = "Имя пользователя";
            }
            else if (table == "VATTable")
            {
                var vat = db.VATTable.Select(n => new { n.VATId, n.Percent }).ToList();
                if (vatPercent == true)
                {
                    vat = db.VATTable.OrderBy(m => m.Percent).Select(n => new { n.VATId, n.Percent }).ToList();
                    vatPercent = false;
                }
                else
                {
                    vat = db.VATTable.OrderByDescending(m => m.Percent).Select(n => new { n.VATId, n.Percent }).ToList();
                    vatPercent = true;
                }
                dgvProductShop.DataSource = vat;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Процент";
            }
            else if (table == "VendorTable")
            {
                var vendor = db.VendorTable.Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                if (e.ColumnIndex == 1)
                {
                    if (vendorName == true)
                    {
                        vendor = db.VendorTable.OrderBy(m => m.VendorName)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        vendorName = false;
                    }
                    else
                    {
                        vendor = db.VendorTable.OrderByDescending(m => m.VendorName)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        vendorName = true;
                    }
                }
                if (e.ColumnIndex == 2)
                {
                    if (nameContactPerson == true)
                    {
                        vendor = db.VendorTable.OrderBy(m => m.ContactPerson)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        nameContactPerson = false;
                    }
                    else
                    {
                        vendor = db.VendorTable.OrderByDescending(m => m.ContactPerson)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        nameContactPerson = true;
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (vendorPhoneNumber == true)
                    {
                        vendor = db.VendorTable.OrderBy(m => m.PhonNumber)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        vendorPhoneNumber = false;
                    }
                    else
                    {
                        vendor = db.VendorTable.OrderByDescending(m => m.PhonNumber)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        vendorPhoneNumber = true;
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (vendorAdress == true)
                    {
                        vendor = db.VendorTable.OrderBy(m => m.Adress)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        vendorAdress = false;
                    }
                    else
                    {
                        vendor = db.VendorTable.OrderByDescending(m => m.Adress)
                            .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                        vendorAdress = true;
                    }
                }
                dgvProductShop.DataSource = vendor;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Поставщик";
                dgvProductShop.Columns[2].HeaderText = "Контактное лицо";
                dgvProductShop.Columns[3].HeaderText = "Номер телефона";
                dgvProductShop.Columns[4].HeaderText = "Адрес";
            }
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miQuery7_Click(object sender, EventArgs e)
        {
            var query = db.DeliveryTable.Where(n => n.ShelfLife > DateTime.Now).GroupBy(n => n.ProductTable.CategoryTable.CategoryName) //если я правильно поняла двигаться нужно из таблицы поставки, 
                .Select(m => new { m.Key, Count = m.Count() }).ToList(); //тк у товара которого не поставляли срока годности нет
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Название категории";
            dgvProductShop.Columns[1].HeaderText = "Количество";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery12_Click(object sender, EventArgs e)
        {
            var query = db.ProductTable.GroupBy(n => n.CategoryTable.CategoryName)
                .Select(m => new { m.Key, Count = m.Count() }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Название категории";
            dgvProductShop.Columns[1].HeaderText = "Количество";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery8_Click(object sender, EventArgs e)
        {
            var query = db.SaleTable.GroupBy(n => n.DeliveryTable.ProductTable.CategoryTable.CategoryName)
                .Select(m => new { m.Key, Count = m.Count() }).ToList();
            query = query.Where(n => n.Count == query.Max(k => k.Count)).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Наиболее популярные категории";
            dgvProductShop.Columns[1].HeaderText = "Количество товаров";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery9_Click(object sender, EventArgs e)
        {
            var query = db.SaleTable.GroupBy(n => n.DeliveryTable.ProductTable.ProductName)
                .Select(m => new { m.Key, Count = m.Count() }).ToList();
            query = query.Where(n => n.Count == query.Max(k => k.Count)).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Наиболее популярные продукты";
            dgvProductShop.Columns[1].HeaderText = "Количество";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void miQuery10_Click(object sender, EventArgs e)
        {

        }

        private void miQuery11_Click(object sender, EventArgs e) //понимаю, что это некорректно, но получилось только так :(
        {
            dgvProductShop.DataSource = null;
            dgvProductShop.Columns.Clear();
            dgvProductShop.Rows.Clear();

            var query = db.SaleTable.GroupBy(n => n.CheckId).ToList();
            
            dgvProductShop.Columns.Add("checkNumber", "Номер чека");
            dgvProductShop.Columns.Add("Product", "Товары");

            foreach (var item in query)
            {
                string products = "";
                bool comma = false;

                foreach (var element in item)
                {
                    if (comma)
                    {
                        products += ", ";
                    }

                    products += element.DeliveryTable.ProductTable.ProductName;

                    comma = true;
                }
                dgvProductShop.Rows.Add(item.Key, products);
            }
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miQuery13_Click(object sender, EventArgs e)
        {
            var query = db.DeliveryTable.Where(n => n.UnitPrice < 300)
                .Select(m => new { m.ProductTable.ProductName, m.UnitPrice, m.UnitTable.NameUnit }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Наименование продукта";
            dgvProductShop.Columns[1].HeaderText = "Цена за единицу";
            dgvProductShop.Columns[2].HeaderText = "Единица измерения";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miQuery14_Click(object sender, EventArgs e)
        {
            var query = db.DeliveryTable.Where(n => n.UnitPrice + n.UnitPrice / 100 * n.ProductTable.CategoryTable.VATTable.Percent < 300)
                .Select(m => new { m.ProductTable.ProductName, m.UnitPrice, m.UnitTable.NameUnit, m.ProductTable.CategoryTable.VATTable.Percent, price = m.UnitPrice + m.UnitPrice / 100 * m.ProductTable.CategoryTable.VATTable.Percent }).ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].HeaderText = "Наименование продукта";
            dgvProductShop.Columns[1].HeaderText = "Цена за единицу";
            dgvProductShop.Columns[2].HeaderText = "Единица измерения";
            dgvProductShop.Columns[3].HeaderText = "НДС";
            dgvProductShop.Columns[4].HeaderText = "Цена с учетом НДС";
            dgvProductShop.Columns[4].DefaultCellStyle.Format = "n4";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miQuery15_Click(object sender, EventArgs e)
        {
            int count = db.SaleTable.Count();
            if (count < 20)
            {
                count = 0;
            }
            else
            {
                count -= 20;
            }
            var query = db.SaleTable
                .Select(n => new { n.SaleId, n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.CheckTable.DateCheck, n.Discount, n.VATSale })
                .OrderBy(n => n.SaleId)
                .Skip(count)
                .ToList();
            dgvProductShop.DataSource = query;
            dgvProductShop.Columns[0].Visible = false;
            dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
            dgvProductShop.Columns[2].HeaderText = "Цена без скидки";
            dgvProductShop.Columns[3].HeaderText = "Количество";
            dgvProductShop.Columns[4].HeaderText = "Дата продажи";
            dgvProductShop.Columns[5].HeaderText = "Скидка (%)";
            dgvProductShop.Columns[6].HeaderText = "НДС продажи (%)";
            dgvProductShop.AutoResizeColumns();
            dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miQuery16_Click(object sender, EventArgs e)
        {
            int count = db.SaleTable.Count();
            if (count > 10)
            {
                if(count < 30)
                {
                    count = db.SaleTable.Count() - 10;
                }
                else
                {
                    count = 20;
                }
                var query = db.SaleTable
                    .Select(n => new { n.SaleId, n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.CheckTable.DateCheck, n.Discount, n.VATSale })
                    .OrderBy(n => n.SaleId)
                    .Skip(10)
                    .Take(count)
                    .ToList();
                dgvProductShop.DataSource = query;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Цена без скидки";
                dgvProductShop.Columns[3].HeaderText = "Количество";
                dgvProductShop.Columns[4].HeaderText = "Дата продажи";
                dgvProductShop.Columns[5].HeaderText = "Скидка (%)";
                dgvProductShop.Columns[6].HeaderText = "НДС продажи (%)";
                dgvProductShop.AutoResizeColumns();
                dgvProductShop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.DefaultCellStyle.Font, FontStyle.Bold);
            }
            else
            {
                MessageBox.Show("Недостаточно продаж для выполнения запроса","Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public CheckedListBox chlb_2 = new CheckedListBox();

        private void miFilter_Click(object sender, EventArgs e)
        {
            if (table != "")
            {
                if (pnlFilter.Visible == false)
                {
                    pnlFilter.Visible = true;
                    if (table == "CategoryTable")
                    {
                        var vat = db.VATTable.Select(n => n.Percent).ToList();
                        chlb.Items.Clear();
                        foreach (byte v in vat)
                        {
                            chlb.Items.Add(v);
                        }
                        lblHeader.Text = "НДС (%): ";
                    }
                    else if (table == "ProductTable")
                    {
                        var category = db.CategoryTable.Select(n => n.CategoryName).ToList();
                        var vendor = db.VendorTable.Select(n => n.VendorName).ToList();
                        chlb.Items.Clear();
                        foreach (string v in category)
                        {
                            chlb.Items.Add(v);
                        }
                        lblHeader.Text = "Категории: ";
                        lblHeader_2.Text = "Поставщики: ";

                        foreach (string v in vendor)
                        {
                            if (!chlb_2.Items.Contains(v))
                            {
                                chlb_2.Items.Add(v);
                            }
                        }
                    }
                    else if (table == "VendorTable")
                    {
                        var adress = db.VendorTable.Select(n => n.Adress).ToList();
                        chlb.Items.Clear();
                        lblHeader.Text = "Город: ";

                        foreach (string v in adress)
                        {
                            if (!chlb.Items.Contains(v.Substring(0, v.IndexOf(','))))
                            {
                                chlb.Items.Add(v.Substring(0, v.IndexOf(',')));
                            }
                        }
                    }
                    else if (table == "UserTable")
                    {
                        var role = db.RoleTable.Select(n => n.NameRole).ToList();
                        chlb.Items.Clear();
                        lblHeader.Text = "Роли: ";
                        foreach (string v in role)
                        {
                            chlb.Items.Add(v);
                        }
                    }
                    else if (table == "DeliveryTable" || table == "SaleTable")
                    {
                        var nameProduct = db.ProductTable.Select(n => n.ProductName).ToList();
                        chlb.Items.Clear();
                        foreach (string v in nameProduct)
                        {
                            chlb.Items.Add(v);
                        }
                        lblHeader.Text = "Продукты: ";
                    }
                    else if (table == "CheckTable")
                    {
                        var seller = db.UserTable.Select(n => n.NameUser).ToList();
                        chlb.Items.Clear();
                        lblHeader.Text = "Продавцы: ";
                        foreach (string v in seller)
                        {
                            chlb.Items.Add(v);
                        }
                    }

                    if (table == "SaleTable")
                    {
                        var vat = db.VATTable.Select(n => n.Percent).ToList();

                        foreach (byte v in vat)
                        {
                            chlb_2.Items.Add(v);
                        }
                        lblHeader_2.Text = "НДС (%): ";

                        cbBestCoincidence.Visible = true;
                    }

                    if (chlb_2.Items.Count != 0)
                    {
                        this.chlb_2.Location = new System.Drawing.Point(378, 25);
                        this.chlb_2.Size = new System.Drawing.Size(200, 94);
                        pnlFilter.Controls.Add(chlb_2);
                    }
                }
                else
                {
                    pnlFilter.Visible = false;
                    SearchString("");
                }
            }
            else
            {
                MessageBox.Show("Выберите таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string>();

            for (int i = 0; i < chlb.Items.Count; i++)
            {
                if (chlb.GetItemCheckState(i) == CheckState.Checked)
                {
                    list.Add(chlb.Items[i].ToString());
                }
            }

            if (chlb_2.Items.Count != 0)
            {
                for (int i = 0; i < chlb_2.Items.Count; i++)
                {
                    if (chlb_2.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(chlb_2.Items[i].ToString());
                    }
                }
            }

            if (table == "CategoryTable")
            {
                var filter = db.CategoryTable
                   .Where(n => list.Any(m => m.ToString() == n.VATTable.Percent.ToString()))
                   .Select(n => new { n.CategoryId, n.CategoryName, n.VATTable.Percent }).ToList();
                dgvProductShop.DataSource = filter;
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Название категории";
                dgvProductShop.Columns[2].HeaderText = "Ндс (%)";
            }
            else if (table == "ProductTable")
            {
                if (chlb.CheckedItems.Count != 0 && chlb_2.CheckedItems.Count != 0)
                {
                    var filter = db.ProductTable
                       .Where(n => list.Any(m => m.ToString() == n.CategoryTable.CategoryName) && list.Any(m => m.ToString() == n.VendorTable.VendorName))
                       .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                    dgvProductShop.DataSource = filter;
                }
                else
                {
                    var filter = db.ProductTable
                       .Where(n => list.Any(m => m.ToString() == n.CategoryTable.CategoryName) || list.Any(m => m.ToString() == n.VendorTable.VendorName))
                       .Select(n => new { n.ProductId, n.ProductName, n.CategoryTable.CategoryName, n.VendorTable.VendorName }).ToList();
                    dgvProductShop.DataSource = filter;
                }

                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Наименование категории";
                dgvProductShop.Columns[3].HeaderText = "Поставщик";
            }
            else if (table == "VendorTable")
            {
                var filter = db.VendorTable
                   .Where(n => list.Any(m => m.ToString() == n.Adress.ToString().Substring(0, n.Adress.IndexOf(","))))
                   .Select(n => new { n.VendorId, n.VendorName, n.ContactPerson, n.PhonNumber, n.Adress }).ToList();
                dgvProductShop.DataSource = filter;

                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Поставщик";
                dgvProductShop.Columns[2].HeaderText = "Контактное лицо";
                dgvProductShop.Columns[3].HeaderText = "Номер телефона";
                dgvProductShop.Columns[4].HeaderText = "Адрес";
            }
            else if (table == "UserTable")
            {
                var filter = db.UserTable
                   .Where(n => list.Any(m => m.ToString() == n.RoleTable.NameRole))
                   .Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole, n.NameUser }).ToList();
                dgvProductShop.DataSource = filter;

                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Логин";
                dgvProductShop.Columns[2].HeaderText = "Пароль";
                dgvProductShop.Columns[3].HeaderText = "Роль";
                dgvProductShop.Columns[4].HeaderText = "Имя пользователя";
            }
            else if (table == "DeliveryTable")
            {
                var filter = db.DeliveryTable
                   .Where(n => list.Any(m => m.ToString() == n.ProductTable.ProductName))
                   .Select(n => new { n.DeliveryId, n.ProductTable.ProductName, n.DateDelivery, n.UnitPrice, 
                       n.DateOfManufacture, n.ShelfLife, n.CountDelivery, n.UnitTable.NameUnit }).ToList();
                dgvProductShop.DataSource = filter;

                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование";
                dgvProductShop.Columns[2].HeaderText = "Дата поставки";
                dgvProductShop.Columns[3].HeaderText = "Цена за единицу";
                dgvProductShop.Columns[4].HeaderText = "Дата производства";
                dgvProductShop.Columns[5].HeaderText = "Срок годности";
                dgvProductShop.Columns[6].HeaderText = "Количество";
                dgvProductShop.Columns[7].HeaderText = "Единица измерения";
            }
            else if (table == "SaleTable")
            {
                var filter = db.SaleTable
                       .Where(n => list.Any(m => m.ToString() == n.DeliveryTable.ProductTable.ProductName) || list.Any(m => m.ToString() == n.DeliveryTable.ProductTable.CategoryTable.VATTable.Percent.ToString()))
                       .Select(n => new { n.SaleId, n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.CheckTable.DateCheck, n.Discount, n.VATSale });
                

                if (chlb.CheckedItems.Count != 0 && chlb_2.CheckedItems.Count != 0)
                {
                    filter = db.SaleTable
                     .Where(n => list.Any(m => m.ToString() == n.DeliveryTable.ProductTable.ProductName) && list.Any(m => m.ToString() == n.DeliveryTable.ProductTable.CategoryTable.VATTable.Percent.ToString()))
                     .Select(n => new { n.SaleId, n.DeliveryTable.ProductTable.ProductName, n.PriceWithoutDiscount, n.CountSale, n.CheckTable.DateCheck, n.Discount, n.VATSale });
                }

                if (cbBestCoincidence.SelectedIndex == 0)
                {
                    filter = filter.OrderBy(n => n.CountSale);
                }
                else if (cbBestCoincidence.SelectedIndex == 1)
                {
                    filter = filter.OrderByDescending(n => n.CountSale);
                }
                else if (cbBestCoincidence.SelectedIndex == 2)
                {
                    filter = filter.OrderBy(n => n.PriceWithoutDiscount);
                }
                else if (cbBestCoincidence.SelectedIndex == 3)
                {
                    filter = filter.OrderByDescending(n => n.PriceWithoutDiscount);
                }

                dgvProductShop.DataSource = filter.ToList();
                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Наименование продукта";
                dgvProductShop.Columns[2].HeaderText = "Цена без скидки";
                dgvProductShop.Columns[3].HeaderText = "Количество";
                dgvProductShop.Columns[4].HeaderText = "Дата продажи";
                dgvProductShop.Columns[5].HeaderText = "Скидка (%)";
                dgvProductShop.Columns[6].HeaderText = "НДС продажи (%)";
            }
            else if (table == "CheckTable")
            {
                var filter = db.CheckTable
                   .Where(n => list.Any(m => m.ToString() == n.UserTable.NameUser))
                   .Select(n => new { n.CheckId, n.DateCheck, n.UserTable.NameUser}).ToList();
                dgvProductShop.DataSource = filter;

                dgvProductShop.Columns[0].Visible = false;
                dgvProductShop.Columns[1].HeaderText = "Дата";
                dgvProductShop.Columns[2].HeaderText = "Имя продавца";
            }

            if (list.Count == 0)
            {
                SearchString("");
            }

            dgvProductShop.AutoResizeColumns();
            dgvProductShop.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvProductShop.ColumnHeadersDefaultCellStyle.Font = new Font(dgvProductShop.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            dgvProductShop.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void pnlFilter_VisibleChanged(object sender, EventArgs e)
        {
            if (!pnlFilter.Visible)
            {
                chlb_2.Items.Clear();
                pnlFilter.Controls.Remove(chlb_2);
                lblHeader_2.Text = "";
                cbBestCoincidence.Visible = false;
            }
        }

        private void saveWord_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Word.Document Odoc = new Word.Document();
            Odoc.Application.Visible = false;

            object start = 0;
            object end = 0;
            Word.Range tableLocation = Odoc.Range(ref start, ref end);
            Odoc.Tables.Add(tableLocation, dgvProductShop.RowCount + 1, dgvProductShop.ColumnCount - 1);

            Odoc.Tables[1].Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
            Odoc.Tables[1].Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;

            for (int j = 1; j < dgvProductShop.ColumnCount; j++)
            {
                Odoc.Tables[1].Cell(1, j).Range.Text = dgvProductShop.Columns[j].HeaderCell.Value.ToString();
                Odoc.Tables[1].Cell(1, j).Range.Paragraphs.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                Odoc.Tables[1].Cell(1, j).Range.Bold = 5;
            }
            for (int i = 1; i <= dgvProductShop.RowCount; i++)
            {
                for (int j = 1; j < dgvProductShop.ColumnCount; j++)
                {
                    Odoc.Tables[1].Cell(i + 1, j).Range.Text = dgvProductShop[j, i - 1].Value.ToString();
                }
            }

            if (saveWord.FilterIndex == 1)
            {
                Odoc.SaveAs(saveWord.FileName, WdSaveFormat.wdFormatDocumentDefault);
            }
            else if (saveWord.FilterIndex == 2)
            {
                Odoc.SaveAs(saveWord.FileName, WdSaveFormat.wdFormatDocument);
            }
            else if (saveWord.FilterIndex == 3)
            {
                Odoc.SaveAs(saveWord.FileName, WdSaveFormat.wdFormatRTF);
            }
            else
            {
                Odoc.SaveAs(saveWord.FileName, WdSaveFormat.wdFormatFlatXML);
            }
            Odoc.Close();
        }

        private void saveExcel_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Excel.Application exApp = new Excel.Application();
            exApp.Visible = false;
            exApp.Workbooks.Add();
            Worksheet worksheet = (Worksheet)exApp.ActiveSheet;

            for (int j = 1; j < dgvProductShop.ColumnCount; j++)
            {
                worksheet.Cells[1, j] = dgvProductShop.Columns[j].HeaderCell.Value.ToString();
            }
            for (int i = 1; i <= dgvProductShop.RowCount; i++)
            {
                for (int j = 1; j < dgvProductShop.ColumnCount; j++)
                {
                    worksheet.Cells[i + 1, j] = dgvProductShop[j, i - 1].Value.ToString();
                }
            }

            worksheet.Columns.AutoFit();

            if (saveExcel.FilterIndex == 1)
            {
                worksheet.SaveAs(saveExcel.FileName, XlFileFormat.xlOpenXMLStrictWorkbook);
            }
            else if (saveExcel.FilterIndex == 2)
            {
                worksheet.SaveAs(saveExcel.FileName, XlFileFormat.xlHtml);
            }
            
            exApp.Quit();
        }

        private void miWord_Click(object sender, EventArgs e)
        {
            saveWord.FileName = "Документ Microsoft Word";
            if (dgvProductShop.Rows.Count > 0)
            {
                saveWord.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void miExel_Click(object sender, EventArgs e)
        {
            saveExcel.FileName = "Лист Microsoft Excel";
            if (dgvProductShop.Rows.Count > 0)
            {
                saveExcel.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart();
        }
    }
}