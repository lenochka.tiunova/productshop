﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class ProductForm : Form
    {
        public ProductForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbNameProduct.Text != "" && cbNameCategory.Text != "" && cbVendor.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    ProductTable productTable = new ProductTable
                    {
                        ProductName = tbNameProduct.Text,
                        CategoryId = db.CategoryTable.FirstOrDefault(n => n.CategoryName == cbNameCategory.Text).CategoryId,
                        VendorId = db.VendorTable.FirstOrDefault(n => n.VendorName == cbVendor.Text).VendorId
                    };
                    db.ProductTable.Add(productTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var productTable = db.ProductTable.FirstOrDefault(n => n.ProductId == index);
                    productTable.ProductName = tbNameProduct.Text;
                    productTable.CategoryId = db.CategoryTable.FirstOrDefault(n => n.CategoryName == cbNameCategory.Text).CategoryId;
                    productTable.VendorId = db.VendorTable.FirstOrDefault(n => n.VendorName == cbVendor.Text).VendorId;
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            cbNameCategory.DataSource = db.CategoryTable.Select(n => n.CategoryName).ToList();
            cbVendor.DataSource = db.VendorTable.Select(n => n.VendorName).ToList();
            if (this.Text == "Редактирование")
            {
                tbNameProduct.Text = db.ProductTable.FirstOrDefault(n => n.ProductId == index).ProductName;
                cbNameCategory.SelectedItem = db.ProductTable.FirstOrDefault(n => n.ProductId == index).CategoryTable.CategoryName;
                cbVendor.SelectedItem = db.ProductTable.FirstOrDefault(n => n.ProductId == index).VendorTable.VendorName;
            }
        }
    }
}
