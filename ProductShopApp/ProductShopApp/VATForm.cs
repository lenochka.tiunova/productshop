﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class VATForm : Form
    {
        public VATForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Text == "Добавление")
            {
                VATTable VATTable = new VATTable
                {
                    Percent = (byte)nudPercent.Value
                };
                db.VATTable.Add(VATTable);
                db.SaveChanges();
                MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                var VATTable = db.VATTable.FirstOrDefault(n => n.VATId == index);
                VATTable.Percent = (byte)nudPercent.Value;
                db.SaveChanges();
                MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void VATForm_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование")
            {
                nudPercent.Value = db.VATTable.FirstOrDefault(n => n.VATId == index).Percent;
            }
        }
    }
}
