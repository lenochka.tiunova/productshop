﻿namespace ProductShopApp
{
    partial class DeliveryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblDateDelivery = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cbNameProduct = new System.Windows.Forms.ComboBox();
            this.cbUnit = new System.Windows.Forms.ComboBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.lblDateOfManufacture = new System.Windows.Forms.Label();
            this.lblShelfLife = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.nudCount = new System.Windows.Forms.NumericUpDown();
            this.nudUnitPrice = new System.Windows.Forms.NumericUpDown();
            this.dateDelivery = new System.Windows.Forms.DateTimePicker();
            this.dateOfManufacture = new System.Windows.Forms.DateTimePicker();
            this.dateShelfLife = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUnitPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(249, 235);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Отменить";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(70, 235);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblDateDelivery
            // 
            this.lblDateDelivery.AutoSize = true;
            this.lblDateDelivery.Location = new System.Drawing.Point(15, 40);
            this.lblDateDelivery.Name = "lblDateDelivery";
            this.lblDateDelivery.Size = new System.Drawing.Size(83, 13);
            this.lblDateDelivery.TabIndex = 13;
            this.lblDateDelivery.Text = "Дата поставки";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(15, 10);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(83, 13);
            this.lblName.TabIndex = 12;
            this.lblName.Text = "Наименование";
            // 
            // cbNameProduct
            // 
            this.cbNameProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNameProduct.FormattingEnabled = true;
            this.cbNameProduct.Location = new System.Drawing.Point(145, 10);
            this.cbNameProduct.Name = "cbNameProduct";
            this.cbNameProduct.Size = new System.Drawing.Size(233, 21);
            this.cbNameProduct.TabIndex = 17;
            // 
            // cbUnit
            // 
            this.cbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUnit.FormattingEnabled = true;
            this.cbUnit.Location = new System.Drawing.Point(145, 190);
            this.cbUnit.Name = "cbUnit";
            this.cbUnit.Size = new System.Drawing.Size(233, 21);
            this.cbUnit.TabIndex = 22;
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(15, 70);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(92, 13);
            this.lblUnitPrice.TabIndex = 23;
            this.lblUnitPrice.Text = "Цена за единицу";
            // 
            // lblDateOfManufacture
            // 
            this.lblDateOfManufacture.AutoSize = true;
            this.lblDateOfManufacture.Location = new System.Drawing.Point(15, 100);
            this.lblDateOfManufacture.Name = "lblDateOfManufacture";
            this.lblDateOfManufacture.Size = new System.Drawing.Size(107, 13);
            this.lblDateOfManufacture.TabIndex = 24;
            this.lblDateOfManufacture.Text = "Дата производства";
            // 
            // lblShelfLife
            // 
            this.lblShelfLife.AutoSize = true;
            this.lblShelfLife.Location = new System.Drawing.Point(15, 130);
            this.lblShelfLife.Name = "lblShelfLife";
            this.lblShelfLife.Size = new System.Drawing.Size(81, 13);
            this.lblShelfLife.TabIndex = 25;
            this.lblShelfLife.Text = "Срок годности";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(15, 160);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(66, 13);
            this.lblCount.TabIndex = 26;
            this.lblCount.Text = "Количество";
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(15, 190);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(109, 13);
            this.lblUnit.TabIndex = 27;
            this.lblUnit.Text = "Единица измерения";
            // 
            // nudCount
            // 
            this.nudCount.Location = new System.Drawing.Point(145, 160);
            this.nudCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCount.Name = "nudCount";
            this.nudCount.Size = new System.Drawing.Size(233, 20);
            this.nudCount.TabIndex = 28;
            this.nudCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudUnitPrice
            // 
            this.nudUnitPrice.DecimalPlaces = 2;
            this.nudUnitPrice.Location = new System.Drawing.Point(145, 70);
            this.nudUnitPrice.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nudUnitPrice.Name = "nudUnitPrice";
            this.nudUnitPrice.Size = new System.Drawing.Size(233, 20);
            this.nudUnitPrice.TabIndex = 29;
            this.nudUnitPrice.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dateDelivery
            // 
            this.dateDelivery.Location = new System.Drawing.Point(145, 40);
            this.dateDelivery.Name = "dateDelivery";
            this.dateDelivery.Size = new System.Drawing.Size(233, 20);
            this.dateDelivery.TabIndex = 30;
            // 
            // dateOfManufacture
            // 
            this.dateOfManufacture.Location = new System.Drawing.Point(145, 100);
            this.dateOfManufacture.Name = "dateOfManufacture";
            this.dateOfManufacture.Size = new System.Drawing.Size(233, 20);
            this.dateOfManufacture.TabIndex = 31;
            // 
            // dateShelfLife
            // 
            this.dateShelfLife.Location = new System.Drawing.Point(145, 130);
            this.dateShelfLife.Name = "dateShelfLife";
            this.dateShelfLife.Size = new System.Drawing.Size(233, 20);
            this.dateShelfLife.TabIndex = 32;
            // 
            // DeliveryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(416, 285);
            this.Controls.Add(this.dateShelfLife);
            this.Controls.Add(this.dateOfManufacture);
            this.Controls.Add(this.dateDelivery);
            this.Controls.Add(this.nudUnitPrice);
            this.Controls.Add(this.nudCount);
            this.Controls.Add(this.lblUnit);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.lblShelfLife);
            this.Controls.Add(this.lblDateOfManufacture);
            this.Controls.Add(this.lblUnitPrice);
            this.Controls.Add(this.cbUnit);
            this.Controls.Add(this.cbNameProduct);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblDateDelivery);
            this.Controls.Add(this.lblName);
            this.MaximizeBox = false;
            this.Name = "DeliveryForm";
            this.Text = "Добавление";
            this.Load += new System.EventHandler(this.DeliveryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUnitPrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblDateDelivery;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cbNameProduct;
        private System.Windows.Forms.ComboBox cbUnit;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.Label lblDateOfManufacture;
        private System.Windows.Forms.Label lblShelfLife;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.NumericUpDown nudCount;
        private System.Windows.Forms.NumericUpDown nudUnitPrice;
        private System.Windows.Forms.DateTimePicker dateDelivery;
        private System.Windows.Forms.DateTimePicker dateOfManufacture;
        private System.Windows.Forms.DateTimePicker dateShelfLife;
    }
}