﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class AuthorizationForm : Form
    {
        dbProductShopEntities db = new dbProductShopEntities();
        public AuthorizationForm()
        {
            InitializeComponent();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            bool login = false;
            var users = db.UserTable.Select(n => new { n.UserId, n.UserLogin, n.UserPassword, n.RoleTable.NameRole }).ToList();
            for (int i = 0; i < users.Count(); i++)
            {
                if (users[i].UserLogin == tbLogin.Text)
                {
                    login = true;
                    break;
                }
            }
            if (login)
            {
                int index = users.FirstOrDefault(n => n.UserLogin == tbLogin.Text).UserId;
                string password = users.FirstOrDefault(n => n.UserId == index).UserPassword.Trim();
                if (password == tbPassword.Text)
                {
                    MessageBox.Show("Вы вошли в систему", "Информация", MessageBoxButtons.OK);
                    if (users.FirstOrDefault(n => n.UserId == index).NameRole != "Администратор")
                    {
                        mainForm main = new mainForm("notAdmin");
                        main.Show();
                    }
                    else
                    {
                        mainForm main = new mainForm("Администратор");
                        main.Show();
                    }
                    this.Visible = false;
                }
                else
                {
                    MessageBox.Show("Неверный пароль", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Пользователь не существует", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
