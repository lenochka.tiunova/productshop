﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class BasketForm : Form
    {
        public BasketForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cbNameProduct.Text != "" && nudCount.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    BasketTable basketTable = new BasketTable
                    {
                        DeliveryId = db.DeliveryTable.FirstOrDefault(n => n.ProductTable.ProductName == cbNameProduct.Text).DeliveryId,
                        CountBasket = (int)nudCount.Value
                    };
                    db.BasketTable.Add(basketTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var basketTable = db.BasketTable.FirstOrDefault(n => n.BasketId == index);
                    basketTable.DeliveryId = db.DeliveryTable.FirstOrDefault(n => n.ProductTable.ProductName == cbNameProduct.Text).DeliveryId;
                    basketTable.CountBasket = (int)nudCount.Value;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BasketForm_Load(object sender, EventArgs e)
        {
            cbNameProduct.DataSource = db.DeliveryTable.Select(n => n.ProductTable.ProductName).ToList();
            if (this.Text == "Редактирование")
            {
                cbNameProduct.SelectedItem = db.BasketTable.FirstOrDefault(n => n.BasketId == index).DeliveryTable.ProductTable.ProductName;
                nudCount.Value = db.BasketTable.FirstOrDefault(n => n.BasketId == index).CountBasket;
            }
        }
    }
}
