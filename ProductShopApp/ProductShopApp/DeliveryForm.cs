﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class DeliveryForm : Form
    {
        public DeliveryForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void DeliveryForm_Load(object sender, EventArgs e)
        {
            cbNameProduct.DataSource = db.ProductTable.Select(n => n.ProductName).ToList();
            cbUnit.DataSource = db.UnitTable.Select(n => n.NameUnit).ToList();
            if (this.Text == "Редактирование")
            {
                cbNameProduct.SelectedItem = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).ProductTable.ProductName;
                dateDelivery.Value = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).DateDelivery;
                nudUnitPrice.Value = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).UnitPrice;
                dateOfManufacture.Value = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).DateOfManufacture;
                dateShelfLife.Value = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).ShelfLife;
                nudCount.Value = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).CountDelivery;
                cbUnit.SelectedItem = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index).UnitTable.NameUnit;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Text == "Добавление")
            {
                DeliveryTable deliveryTable = new DeliveryTable
                {
                    ProductId = db.ProductTable.FirstOrDefault(n => n.ProductName == cbNameProduct.Text).ProductId,
                    DateDelivery = dateDelivery.Value,
                    UnitPrice = nudUnitPrice.Value,
                    DateOfManufacture = dateOfManufacture.Value,
                    ShelfLife = dateShelfLife.Value,
                    CountDelivery = (int)nudCount.Value,
                    UnitId = db.UnitTable.FirstOrDefault(n => n.NameUnit == cbUnit.Text).UnitId
                };
                db.DeliveryTable.Add(deliveryTable);
                db.SaveChanges();
                MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                var deliveryTable = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index);
                deliveryTable.ProductId = db.ProductTable.FirstOrDefault(n => n.ProductName == cbNameProduct.Text).ProductId;
                deliveryTable.DateDelivery = dateDelivery.Value;
                deliveryTable.UnitPrice = nudUnitPrice.Value;
                deliveryTable.DateOfManufacture = dateOfManufacture.Value;
                deliveryTable.ShelfLife = dateShelfLife.Value;
                deliveryTable.CountDelivery = (int)nudCount.Value;
                deliveryTable.UnitId = db.UnitTable.FirstOrDefault(n => n.NameUnit == cbUnit.Text).UnitId;
                db.SaveChanges();
                MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}
