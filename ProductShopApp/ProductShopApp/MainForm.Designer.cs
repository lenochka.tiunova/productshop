﻿namespace ProductShopApp
{
    partial class mainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.menuProductShop = new System.Windows.Forms.MenuStrip();
            this.miTables = new System.Windows.Forms.ToolStripMenuItem();
            this.miCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.miProducts = new System.Windows.Forms.ToolStripMenuItem();
            this.miVAT = new System.Windows.Forms.ToolStripMenuItem();
            this.miVendors = new System.Windows.Forms.ToolStripMenuItem();
            this.miUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.miRoles = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelivery = new System.Windows.Forms.ToolStripMenuItem();
            this.miBasket = new System.Windows.Forms.ToolStripMenuItem();
            this.miUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.miSales = new System.Windows.Forms.ToolStripMenuItem();
            this.miCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.cc = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miRequests = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery3 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery4 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery5 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery6 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery7 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery8 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery9 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery10 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery11 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery12 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery13 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery14 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery15 = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuery16 = new System.Windows.Forms.ToolStripMenuItem();
            this.miReports = new System.Windows.Forms.ToolStripMenuItem();
            this.miBasketMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.miFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.miWord = new System.Windows.Forms.ToolStripMenuItem();
            this.miExel = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvProductShop = new System.Windows.Forms.DataGridView();
            this.lblSearch = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.cbBestCoincidence = new System.Windows.Forms.ComboBox();
            this.lblHeader_2 = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.chlb = new System.Windows.Forms.CheckedListBox();
            this.saveWord = new System.Windows.Forms.SaveFileDialog();
            this.saveExcel = new System.Windows.Forms.SaveFileDialog();
            this.menuProductShop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductShop)).BeginInit();
            this.pnlFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuProductShop
            // 
            this.menuProductShop.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuProductShop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miTables,
            this.miEditMenu,
            this.miRequests,
            this.miReports,
            this.miBasketMenu,
            this.miFilter,
            this.miExport,
            this.miExit});
            this.menuProductShop.Location = new System.Drawing.Point(0, 0);
            this.menuProductShop.Name = "menuProductShop";
            this.menuProductShop.Size = new System.Drawing.Size(898, 24);
            this.menuProductShop.TabIndex = 0;
            // 
            // miTables
            // 
            this.miTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCategory,
            this.miProducts,
            this.miVAT,
            this.miVendors,
            this.miUsers,
            this.miRoles,
            this.miDelivery,
            this.miBasket,
            this.miUnit,
            this.miSales,
            this.miCheck});
            this.miTables.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.miTables.Name = "miTables";
            this.miTables.Size = new System.Drawing.Size(68, 20);
            this.miTables.Text = "Таблицы";
            // 
            // miCategory
            // 
            this.miCategory.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miCategory.Name = "miCategory";
            this.miCategory.Size = new System.Drawing.Size(186, 22);
            this.miCategory.Text = "Категории";
            this.miCategory.Click += new System.EventHandler(this.miCategory_Click);
            // 
            // miProducts
            // 
            this.miProducts.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miProducts.Name = "miProducts";
            this.miProducts.Size = new System.Drawing.Size(186, 22);
            this.miProducts.Text = "Продукты";
            this.miProducts.Click += new System.EventHandler(this.miProducts_Click);
            // 
            // miVAT
            // 
            this.miVAT.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miVAT.Name = "miVAT";
            this.miVAT.Size = new System.Drawing.Size(186, 22);
            this.miVAT.Text = "НДС";
            this.miVAT.Click += new System.EventHandler(this.miVAT_Click);
            // 
            // miVendors
            // 
            this.miVendors.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miVendors.Name = "miVendors";
            this.miVendors.Size = new System.Drawing.Size(186, 22);
            this.miVendors.Text = "Продавцы";
            this.miVendors.Click += new System.EventHandler(this.miVendors_Click);
            // 
            // miUsers
            // 
            this.miUsers.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miUsers.Name = "miUsers";
            this.miUsers.Size = new System.Drawing.Size(186, 22);
            this.miUsers.Text = "Пользователи";
            this.miUsers.Click += new System.EventHandler(this.miUsers_Click);
            // 
            // miRoles
            // 
            this.miRoles.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miRoles.Name = "miRoles";
            this.miRoles.Size = new System.Drawing.Size(186, 22);
            this.miRoles.Text = "Роли";
            this.miRoles.Click += new System.EventHandler(this.miRoles_Click);
            // 
            // miDelivery
            // 
            this.miDelivery.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miDelivery.Name = "miDelivery";
            this.miDelivery.Size = new System.Drawing.Size(186, 22);
            this.miDelivery.Text = "Поставка";
            this.miDelivery.Click += new System.EventHandler(this.miDelivery_Click);
            // 
            // miBasket
            // 
            this.miBasket.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miBasket.Name = "miBasket";
            this.miBasket.Size = new System.Drawing.Size(186, 22);
            this.miBasket.Text = "Корзина";
            this.miBasket.Click += new System.EventHandler(this.miBasket_Click);
            // 
            // miUnit
            // 
            this.miUnit.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miUnit.Name = "miUnit";
            this.miUnit.Size = new System.Drawing.Size(186, 22);
            this.miUnit.Text = "Единицы измерения";
            this.miUnit.Click += new System.EventHandler(this.miUnit_Click);
            // 
            // miSales
            // 
            this.miSales.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miSales.Name = "miSales";
            this.miSales.Size = new System.Drawing.Size(186, 22);
            this.miSales.Text = "Продажи";
            this.miSales.Click += new System.EventHandler(this.miSales_Click);
            // 
            // miCheck
            // 
            this.miCheck.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miCheck.Name = "miCheck";
            this.miCheck.Size = new System.Drawing.Size(186, 22);
            this.miCheck.Text = "Чек";
            this.miCheck.Click += new System.EventHandler(this.miCheck_Click);
            // 
            // miEditMenu
            // 
            this.miEditMenu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.miEditMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cc,
            this.miEdit,
            this.miDelete});
            this.miEditMenu.Name = "miEditMenu";
            this.miEditMenu.Size = new System.Drawing.Size(108, 20);
            this.miEditMenu.Text = "Редактирование";
            // 
            // cc
            // 
            this.cc.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cc.Name = "cc";
            this.cc.Size = new System.Drawing.Size(180, 22);
            this.cc.Text = "Добавить";
            this.cc.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miEdit
            // 
            this.miEdit.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(180, 22);
            this.miEdit.Text = "Редактировать";
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(180, 22);
            this.miDelete.Text = "Удалить";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // miRequests
            // 
            this.miRequests.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.miRequests.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miQuery1,
            this.miQuery2,
            this.miQuery3,
            this.miQuery4,
            this.miQuery5,
            this.miQuery6,
            this.miQuery7,
            this.miQuery8,
            this.miQuery9,
            this.miQuery10,
            this.miQuery11,
            this.miQuery12,
            this.miQuery13,
            this.miQuery14,
            this.miQuery15,
            this.miQuery16});
            this.miRequests.Name = "miRequests";
            this.miRequests.Size = new System.Drawing.Size(68, 20);
            this.miRequests.Text = "Запросы";
            // 
            // miQuery1
            // 
            this.miQuery1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery1.Name = "miQuery1";
            this.miQuery1.Size = new System.Drawing.Size(142, 22);
            this.miQuery1.Text = "Запрос №1";
            this.miQuery1.Click += new System.EventHandler(this.miQuery1_Click);
            // 
            // miQuery2
            // 
            this.miQuery2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery2.Name = "miQuery2";
            this.miQuery2.Size = new System.Drawing.Size(142, 22);
            this.miQuery2.Text = "Запрос №2";
            this.miQuery2.Click += new System.EventHandler(this.miQuery2_Click);
            // 
            // miQuery3
            // 
            this.miQuery3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery3.Name = "miQuery3";
            this.miQuery3.Size = new System.Drawing.Size(142, 22);
            this.miQuery3.Text = "Запрос №3";
            this.miQuery3.Click += new System.EventHandler(this.miQuery3_Click);
            // 
            // miQuery4
            // 
            this.miQuery4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery4.Name = "miQuery4";
            this.miQuery4.Size = new System.Drawing.Size(142, 22);
            this.miQuery4.Text = "Запрос №4";
            this.miQuery4.Click += new System.EventHandler(this.miQuery4_Click);
            // 
            // miQuery5
            // 
            this.miQuery5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery5.Name = "miQuery5";
            this.miQuery5.Size = new System.Drawing.Size(142, 22);
            this.miQuery5.Text = "Запрос №5";
            this.miQuery5.Click += new System.EventHandler(this.miQuery5_Click);
            // 
            // miQuery6
            // 
            this.miQuery6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery6.Name = "miQuery6";
            this.miQuery6.Size = new System.Drawing.Size(142, 22);
            this.miQuery6.Text = "Запрос №6";
            this.miQuery6.Click += new System.EventHandler(this.miQuery6_Click);
            // 
            // miQuery7
            // 
            this.miQuery7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery7.Name = "miQuery7";
            this.miQuery7.Size = new System.Drawing.Size(142, 22);
            this.miQuery7.Text = "Запрос №7";
            this.miQuery7.Click += new System.EventHandler(this.miQuery7_Click);
            // 
            // miQuery8
            // 
            this.miQuery8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery8.Name = "miQuery8";
            this.miQuery8.Size = new System.Drawing.Size(142, 22);
            this.miQuery8.Text = "Запрос №8";
            this.miQuery8.Click += new System.EventHandler(this.miQuery8_Click);
            // 
            // miQuery9
            // 
            this.miQuery9.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery9.Name = "miQuery9";
            this.miQuery9.Size = new System.Drawing.Size(142, 22);
            this.miQuery9.Text = "Запрос №9";
            this.miQuery9.Click += new System.EventHandler(this.miQuery9_Click);
            // 
            // miQuery10
            // 
            this.miQuery10.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery10.Name = "miQuery10";
            this.miQuery10.Size = new System.Drawing.Size(142, 22);
            this.miQuery10.Text = "Запрос №10";
            this.miQuery10.Click += new System.EventHandler(this.miQuery10_Click);
            // 
            // miQuery11
            // 
            this.miQuery11.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery11.Name = "miQuery11";
            this.miQuery11.Size = new System.Drawing.Size(142, 22);
            this.miQuery11.Text = "Запрос №11";
            this.miQuery11.Click += new System.EventHandler(this.miQuery11_Click);
            // 
            // miQuery12
            // 
            this.miQuery12.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery12.Name = "miQuery12";
            this.miQuery12.Size = new System.Drawing.Size(142, 22);
            this.miQuery12.Text = "Запрос №12";
            this.miQuery12.Click += new System.EventHandler(this.miQuery12_Click);
            // 
            // miQuery13
            // 
            this.miQuery13.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery13.Name = "miQuery13";
            this.miQuery13.Size = new System.Drawing.Size(142, 22);
            this.miQuery13.Text = "Запрос №13";
            this.miQuery13.Click += new System.EventHandler(this.miQuery13_Click);
            // 
            // miQuery14
            // 
            this.miQuery14.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery14.Name = "miQuery14";
            this.miQuery14.Size = new System.Drawing.Size(142, 22);
            this.miQuery14.Text = "Запрос №14";
            this.miQuery14.Click += new System.EventHandler(this.miQuery14_Click);
            // 
            // miQuery15
            // 
            this.miQuery15.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery15.Name = "miQuery15";
            this.miQuery15.Size = new System.Drawing.Size(142, 22);
            this.miQuery15.Text = "Запрос №15";
            this.miQuery15.Click += new System.EventHandler(this.miQuery15_Click);
            // 
            // miQuery16
            // 
            this.miQuery16.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.miQuery16.Name = "miQuery16";
            this.miQuery16.Size = new System.Drawing.Size(142, 22);
            this.miQuery16.Text = "Запрос №16";
            this.miQuery16.Click += new System.EventHandler(this.miQuery16_Click);
            // 
            // miReports
            // 
            this.miReports.Name = "miReports";
            this.miReports.Size = new System.Drawing.Size(60, 20);
            this.miReports.Text = "Отчеты";
            this.miReports.Click += new System.EventHandler(this.miReports_Click);
            // 
            // miBasketMenu
            // 
            this.miBasketMenu.Name = "miBasketMenu";
            this.miBasketMenu.Size = new System.Drawing.Size(65, 20);
            this.miBasketMenu.Text = "Корзина";
            this.miBasketMenu.Click += new System.EventHandler(this.miBasketMenu_Click);
            // 
            // miFilter
            // 
            this.miFilter.Name = "miFilter";
            this.miFilter.Size = new System.Drawing.Size(60, 20);
            this.miFilter.Text = "Фильтр";
            this.miFilter.Click += new System.EventHandler(this.miFilter_Click);
            // 
            // miExport
            // 
            this.miExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWord,
            this.miExel});
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(64, 20);
            this.miExport.Text = "Экспорт";
            // 
            // miWord
            // 
            this.miWord.Name = "miWord";
            this.miWord.Size = new System.Drawing.Size(180, 22);
            this.miWord.Text = "Word";
            this.miWord.Click += new System.EventHandler(this.miWord_Click);
            // 
            // miExel
            // 
            this.miExel.Name = "miExel";
            this.miExel.Size = new System.Drawing.Size(180, 22);
            this.miExel.Text = "Excel";
            this.miExel.Click += new System.EventHandler(this.miExel_Click);
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(120, 20);
            this.miExit.Text = "Выйти из аккаунта";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // dgvProductShop
            // 
            this.dgvProductShop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProductShop.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvProductShop.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProductShop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductShop.Location = new System.Drawing.Point(3, 60);
            this.dgvProductShop.Name = "dgvProductShop";
            this.dgvProductShop.Size = new System.Drawing.Size(895, 353);
            this.dgvProductShop.TabIndex = 1;
            this.dgvProductShop.TabStop = false;
            this.dgvProductShop.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProductShop_ColumnHeaderMouseClick);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(5, 34);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(39, 13);
            this.lblSearch.TabIndex = 2;
            this.lblSearch.Text = "Поиск";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Location = new System.Drawing.Point(803, 31);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 20);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Найти";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(54, 31);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(733, 20);
            this.tbSearch.TabIndex = 4;
            // 
            // pnlFilter
            // 
            this.pnlFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlFilter.Controls.Add(this.cbBestCoincidence);
            this.pnlFilter.Controls.Add(this.lblHeader_2);
            this.pnlFilter.Controls.Add(this.lblHeader);
            this.pnlFilter.Controls.Add(this.btnFilter);
            this.pnlFilter.Controls.Add(this.chlb);
            this.pnlFilter.Location = new System.Drawing.Point(8, 420);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(890, 128);
            this.pnlFilter.TabIndex = 5;
            this.pnlFilter.Visible = false;
            this.pnlFilter.VisibleChanged += new System.EventHandler(this.pnlFilter_VisibleChanged);
            // 
            // cbBestCoincidence
            // 
            this.cbBestCoincidence.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.cbBestCoincidence.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBestCoincidence.FormattingEnabled = true;
            this.cbBestCoincidence.Items.AddRange(new object[] {
            "Количество (по возрастанию)",
            "Количество (по убыванию)",
            "Цена (по возрастанию)",
            "Цена (по убыванию)"});
            this.cbBestCoincidence.Location = new System.Drawing.Point(600, 57);
            this.cbBestCoincidence.Name = "cbBestCoincidence";
            this.cbBestCoincidence.Size = new System.Drawing.Size(154, 21);
            this.cbBestCoincidence.TabIndex = 9;
            this.cbBestCoincidence.Text = "Лучшее совпадение";
            this.cbBestCoincidence.Visible = false;
            // 
            // lblHeader_2
            // 
            this.lblHeader_2.AutoSize = true;
            this.lblHeader_2.Location = new System.Drawing.Point(375, 8);
            this.lblHeader_2.Name = "lblHeader_2";
            this.lblHeader_2.Size = new System.Drawing.Size(0, 13);
            this.lblHeader_2.TabIndex = 8;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(14, 8);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(0, 13);
            this.lblHeader.TabIndex = 7;
            // 
            // btnFilter
            // 
            this.btnFilter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnFilter.FlatAppearance.BorderSize = 0;
            this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilter.Location = new System.Drawing.Point(274, 56);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(98, 23);
            this.btnFilter.TabIndex = 6;
            this.btnFilter.Text = "Фильтровать";
            this.btnFilter.UseVisualStyleBackColor = false;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // chlb
            // 
            this.chlb.FormattingEnabled = true;
            this.chlb.Location = new System.Drawing.Point(17, 25);
            this.chlb.Name = "chlb";
            this.chlb.Size = new System.Drawing.Size(251, 94);
            this.chlb.TabIndex = 0;
            // 
            // saveWord
            // 
            this.saveWord.FileOk += new System.ComponentModel.CancelEventHandler(this.saveWord_FileOk);
            // 
            // saveExcel
            // 
            this.saveExcel.FileOk += new System.ComponentModel.CancelEventHandler(this.saveExcel_FileOk);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(898, 551);
            this.Controls.Add(this.pnlFilter);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.dgvProductShop);
            this.Controls.Add(this.menuProductShop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuProductShop;
            this.MinimumSize = new System.Drawing.Size(914, 590);
            this.Name = "mainForm";
            this.Text = "Магазин продуктов";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
            this.menuProductShop.ResumeLayout(false);
            this.menuProductShop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductShop)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuProductShop;
        private System.Windows.Forms.DataGridView dgvProductShop;
        private System.Windows.Forms.ToolStripMenuItem miVendors;
        private System.Windows.Forms.ToolStripMenuItem miUsers;
        private System.Windows.Forms.ToolStripMenuItem miRoles;
        private System.Windows.Forms.ToolStripMenuItem miDelivery;
        private System.Windows.Forms.ToolStripMenuItem miBasket;
        private System.Windows.Forms.ToolStripMenuItem miUnit;
        private System.Windows.Forms.ToolStripMenuItem miSales;
        private System.Windows.Forms.ToolStripMenuItem miCheck;
        private System.Windows.Forms.ToolStripMenuItem miQuery1;
        private System.Windows.Forms.ToolStripMenuItem miQuery2;
        private System.Windows.Forms.ToolStripMenuItem miQuery3;
        private System.Windows.Forms.ToolStripMenuItem miQuery4;
        private System.Windows.Forms.ToolStripMenuItem miQuery5;
        private System.Windows.Forms.ToolStripMenuItem miQuery6;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.ToolStripMenuItem cc;
        private System.Windows.Forms.ToolStripMenuItem miQuery7;
        private System.Windows.Forms.ToolStripMenuItem miQuery8;
        private System.Windows.Forms.ToolStripMenuItem miQuery9;
        private System.Windows.Forms.ToolStripMenuItem miQuery10;
        private System.Windows.Forms.ToolStripMenuItem miQuery11;
        private System.Windows.Forms.ToolStripMenuItem miQuery12;
        private System.Windows.Forms.ToolStripMenuItem miQuery13;
        private System.Windows.Forms.ToolStripMenuItem miQuery14;
        private System.Windows.Forms.ToolStripMenuItem miQuery15;
        private System.Windows.Forms.ToolStripMenuItem miQuery16;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.CheckedListBox chlb;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblHeader_2;
        private System.Windows.Forms.ComboBox cbBestCoincidence;
        private System.Windows.Forms.SaveFileDialog saveWord;
        private System.Windows.Forms.SaveFileDialog saveExcel;
        private System.Windows.Forms.ToolStripMenuItem miWord;
        private System.Windows.Forms.ToolStripMenuItem miExel;
        public System.Windows.Forms.ToolStripMenuItem miTables;
        public System.Windows.Forms.ToolStripMenuItem miRequests;
        public System.Windows.Forms.ToolStripMenuItem miReports;
        public System.Windows.Forms.ToolStripMenuItem miEditMenu;
        public System.Windows.Forms.ToolStripMenuItem miEdit;
        public System.Windows.Forms.ToolStripMenuItem miDelete;
        public System.Windows.Forms.ToolStripMenuItem miBasketMenu;
        public System.Windows.Forms.ToolStripMenuItem miFilter;
        public System.Windows.Forms.ToolStripMenuItem miExport;
        public System.Windows.Forms.ToolStripMenuItem miCategory;
        public System.Windows.Forms.ToolStripMenuItem miProducts;
        public System.Windows.Forms.ToolStripMenuItem miVAT;
        private System.Windows.Forms.ToolStripMenuItem miExit;
    }
}

