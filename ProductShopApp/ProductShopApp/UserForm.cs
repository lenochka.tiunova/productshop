﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbLogin.Text != "" && tbPassword.Text != "" && cbRole.Text != "" && tbUserName.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    UserTable userTable = new UserTable
                    {
                        UserLogin = tbLogin.Text,
                        UserPassword = tbPassword.Text,
                        RoleId = db.RoleTable.FirstOrDefault(n => n.NameRole == cbRole.Text).RoleId,
                        NameUser = tbUserName.Text
                    };
                    db.UserTable.Add(userTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var userTable = db.UserTable.FirstOrDefault(n => n.UserId == index);
                    userTable.UserLogin = tbLogin.Text;
                    userTable.UserPassword = tbPassword.Text;
                    userTable.RoleId = db.RoleTable.FirstOrDefault(n => n.NameRole == cbRole.Text).RoleId;
                    userTable.NameUser = tbUserName.Text;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            cbRole.DataSource = db.RoleTable.Select(n => n.NameRole).ToList();
            if (this.Text == "Редактирование")
            {
                tbLogin.Text = db.UserTable.FirstOrDefault(n => n.UserId == index).UserLogin;
                tbPassword.Text = db.UserTable.FirstOrDefault(n => n.UserId == index).UserPassword;
                cbRole.Text = db.UserTable.FirstOrDefault(n => n.UserId == index).RoleTable.NameRole;
                tbUserName.Text = db.UserTable.FirstOrDefault(n => n.UserId == index).NameUser;
            }
        }
    }
}