﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class VendorForm : Form
    {
        public VendorForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbNameVendor.Text != "" && tbContactPerson.Text != "" && tbNumberPhone.Text != "" && tbAdress.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    VendorTable vendorTable = new VendorTable
                    {
                        VendorName = tbNameVendor.Text,
                        ContactPerson = tbContactPerson.Text,
                        PhonNumber = tbNumberPhone.Text,
                        Adress = tbAdress.Text
                    };
                    db.VendorTable.Add(vendorTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var vendorTable = db.VendorTable.FirstOrDefault(n => n.VendorId == index);
                    vendorTable.VendorName = tbNameVendor.Text;
                    vendorTable.ContactPerson = tbContactPerson.Text;
                    vendorTable.PhonNumber = tbNumberPhone.Text;
                    vendorTable.Adress = tbAdress.Text;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void VendorForm_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование")
            {
                tbNameVendor.Text = db.VendorTable.FirstOrDefault(n => n.VendorId == index).VendorName;
                tbContactPerson.Text = db.VendorTable.FirstOrDefault(n => n.VendorId == index).ContactPerson;
                tbNumberPhone.Text = db.VendorTable.FirstOrDefault(n => n.VendorId == index).PhonNumber;
                tbAdress.Text = db.VendorTable.FirstOrDefault(n => n.VendorId == index).Adress;
            }
        }
    }
}
